package com.stylefeng.guns.core.util;

public class ConstantParams {

    public static final String 报文_报文头 = "ReportHeader";
    public static final String 报文_报文体 = "ReportBody";
    public static final String 报文头_信函类型 = "ReportType";
    public static final String 报文头_发送时间 = "SendTime";
    public static final String 报文头_渠道 = "Channel";
}
