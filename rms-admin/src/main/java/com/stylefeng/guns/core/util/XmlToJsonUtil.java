package com.stylefeng.guns.core.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.dom4j.*;

import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XmlToJsonUtil {
    public static void main(String[] args) throws Exception {
//        String filePath = "C:\\Users\\Administrator\\Desktop\\核保通知函--1.xml";
        String filePath = "C:\\Users\\Administrator\\Desktop\\test.xml";
        String xmlStr = readFile(filePath);
        xmlStr = filterCdataTag(xmlStr);
        System.out.println(xmlStr);
        Document doc = DocumentHelper.parseText(xmlStr);
        JSONObject json = new JSONObject();
        dom4j2Json(doc.getRootElement(), json);
        System.out.println("xml2Json:" + json.toJSONString());

    }

    /**
     * 过滤 CData标签
     *
     * @param s
     */
    public static String filterCdataTag(String s) {
        String regex = "<!\\[CDATA\\[.*?\\]\\]>";
        Matcher mt = Pattern.compile(regex).matcher(s);//此处为改动部分
        while (mt.find()) {
            String LRCurl = mt.group(0);//此处为改动部分
            System.out.println(LRCurl + " ---- " + LRCurl.replace("<![CDATA[\"", "").replace("\"]]>", ""));
            s = s.replace(LRCurl, LRCurl.replace("<![CDATA[\"", "").replace("\"]]>", ""));
        }
        return s;
    }

    /**
     * 过滤 空节点
     *
     * @param s
     */
    public static String filterSpace(String s) {
        String regex = "<([^>]+)>\\s*</\\1>";
        Matcher mt = Pattern.compile(regex).matcher(s);//此处为改动部分
        while (mt.find()) {
            String LRCurl = mt.group(0);//取得空节点
            s = s.replace(LRCurl, "");
            mt = Pattern.compile(regex).matcher(s);
            while (mt.find()) {
                LRCurl = mt.group(0);//取得空节点
                s = s.replace(LRCurl, "");
            }
        }
        return s;
    }

    /**
     * 将xml 转换为json
     *
     * @param xmlStr
     * @return
     */
    public static JSONObject transXml2Json(String xmlStr) throws DocumentException {
        String xmlStrTemp = xmlStr;
        xmlStrTemp = filterSpace(xmlStrTemp);
        xmlStrTemp = filterCdataTag(xmlStrTemp);
        Document doc = DocumentHelper.parseText(xmlStrTemp);
        JSONObject json = new JSONObject();
        dom4j2Json(doc.getRootElement(), json);
        return json;
    }

    /**
     * 将xml 转换为json
     *
     * @param xmlFilePath
     * @return
     */
    public static JSONObject transXmlFile2Json(String xmlFilePath) throws DocumentException {
        JSONObject json = new JSONObject();
        try {
            String xmlStrTemp = readFile(xmlFilePath);
            xmlStrTemp = filterSpace(xmlStrTemp);
            xmlStrTemp = filterCdataTag(xmlStrTemp);
            Document doc = DocumentHelper.parseText(xmlStrTemp);
            dom4j2Json(doc.getRootElement(), json);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return json;
    }

    public static String readFile(String path) throws Exception {
        File file = new File(path);
        FileInputStream fis = new FileInputStream(file);
        FileChannel fc = fis.getChannel();
        ByteBuffer bb = ByteBuffer.allocate(new Long(file.length()).intValue());
        //fc向buffer中读入数据
        fc.read(bb);
        bb.flip();
        String str = new String(bb.array(), "UTF-8");
        fc.close();
        fis.close();
        return str;

    }

    /**
     * xml转json
     *
     * @param xmlStr
     * @return
     * @throws DocumentException
     */
    public static JSONObject xml2Json(String xmlStr) throws DocumentException {
        Document doc = DocumentHelper.parseText(xmlStr);
        JSONObject json = new JSONObject();
        dom4j2Json(doc.getRootElement(), json);
        return json;
    }

    /**
     * xml转json
     *
     * @param element
     * @param json
     */
    public static void dom4j2Json(Element element, JSONObject json) {
        //如果是属性
        for (Object o : element.attributes()) {
            Attribute attr = (Attribute) o;
            if (!isEmpty(attr.getValue())) {
                json.put("@" + attr.getName(), attr.getValue());
            }
        }
        List<Element> chdEl = element.elements();
        if (chdEl.isEmpty() && !isEmpty(element.getText())) {//如果没有子元素,只有一个值
            json.put(element.getName(), element.getText());
        }

        for (Element e : chdEl) {//有子元素
            if (!e.elements().isEmpty()) {//子元素也有子元素
                JSONObject chdjson = new JSONObject();
                dom4j2Json(e, chdjson);
                Object o = json.get(e.getName());
                if (o != null) {
                    JSONArray jsona = null;
                    if (o instanceof JSONObject) {//如果此元素已存在,则转为jsonArray
                        JSONObject jsono = (JSONObject) o;
                        json.remove(e.getName());
                        jsona = new JSONArray();
                        jsona.add(jsono);
                        jsona.add(chdjson);
                    }
                    if (o instanceof JSONArray) {
                        jsona = (JSONArray) o;
                        jsona.add(chdjson);
                    }
                    json.put(e.getName(), jsona);
                } else {
//                    if (!chdjson.isEmpty()) {
                    json.put(e.getName(), chdjson);
//                    }
                }
            } else {//子元素没有子元素
                for (Object o : element.attributes()) {
                    Attribute attr = (Attribute) o;
                    if (!isEmpty(attr.getValue())) {
                        json.put("@" + attr.getName(), attr.getValue());
                    }
                }
                if (!e.getText().isEmpty()) {
                    json.put(e.getName(), e.getText());
                }
            }
        }
    }

    public static boolean isEmpty(String str) {

        if (str == null || str.trim().isEmpty() || "null".equals(str)) {
            return true;
        }
        return false;
    }
}
