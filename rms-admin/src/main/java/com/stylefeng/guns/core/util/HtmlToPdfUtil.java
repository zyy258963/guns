package com.stylefeng.guns.core.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class HtmlToPdfUtil {


    private static final Logger LOG = LoggerFactory.getLogger(HtmlToPdfUtil.class);

   //public static final String TOPDFTOOL = "F:\\wkhtmltox\\wkhtmltopdf\\bin\\wkhtmltopdf.exe";
     private static final String TOPDFTOOL = "/usr/local/wkhtmltox/bin/wkhtmltopdf";

    /**
     * html转pdf
     *
     * @param srcPath  html路径，可以是硬盘上的路径，也可以是网络路径
     * @param destPath pdf保存路径
     * @return 转换成功返回true
     */
    public static boolean convert(String srcPath, String destPath) {

        File file = new File(destPath);
        File parent = file.getParentFile();
        // 如果pdf保存路径不存在，则创建路径
        if (!parent.exists()) {
            parent.mkdirs();
        }

        StringBuilder cmd = new StringBuilder();
        cmd.append(TOPDFTOOL);
        cmd.append(" ");
        cmd.append("--page-size A4");// 参数
        cmd.append(" ");
        cmd.append(" --margin-top 20 ");
        cmd.append(" ");
        cmd.append("--disable-smart-shrinking  ");
        cmd.append(" ");
        //cmd.append("--no-background ");
        cmd.append(" ");
        cmd.append(srcPath);
        cmd.append(" ");
        cmd.append(destPath);

        boolean result = true;
        try {
            Process proc = Runtime.getRuntime().exec(cmd.toString());
            HtmlToPdfInterceptor error = new HtmlToPdfInterceptor(
                    proc.getErrorStream());
            HtmlToPdfInterceptor output = new HtmlToPdfInterceptor(
                    proc.getInputStream());
            error.start();
            output.start();
            proc.waitFor();
            LOG.info("HTML2PDF成功，参数---html路径：{},pdf保存路径 ：{}", new Object[]{srcPath, destPath});
        } catch (Exception e) {
            LOG.error("HTML2PDF失败，srcPath地址：{},错误信息：{}", new Object[]{srcPath, e.getMessage()});
            result = false;
        }
        return result;
    }

    public static void main(String[] args) {
        String url ="http://localhost:8080/report/show?reportType=UniversalAnnualReport&dataId=995901085196492801";
        convert(url,"F:\\guss\\" + System.currentTimeMillis() + ".pdf");
    }
}
