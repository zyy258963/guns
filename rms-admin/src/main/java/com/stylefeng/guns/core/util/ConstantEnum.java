package com.stylefeng.guns.core.util;

public class ConstantEnum {

    /**
     * 字典的的枚举
     */
    public enum Enum_DictType {
        业务模块("业务模块", "业务模块"), 状态("状态", "状态"), 性别("性别", "性别"), 信函渠道("信函渠道", "信函渠道");

        // 成员变量
        private String name;
        private String code;

        // 构造方法
        private Enum_DictType(String name, String code) {
            this.name = name;
            this.code = code;
        }

        // 普通方法
        public static String getName(String code) {
            for (Enum_DictType c : Enum_DictType.values()) {
                if (code.equals(c.getCode())) {
                    return c.getName();
                }
            }
            return null;
        }

        // 普通方法
        public static String getCode(String name) {
            for (Enum_DictType c : Enum_DictType.values()) {
                if (name.equals(c.getName())) {
                    return c.getCode();
                }
            }
            return null;
        }

        // get set 方法
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }
    }


    /**
     * 字典的的枚举
     */
    public enum Enum_CommonStatus {
        启用("启用", 1), 禁用("禁用", 2);

        // 成员变量
        private String name;
        private Integer code;

        // 构造方法
        private Enum_CommonStatus(String name, Integer code) {
            this.name = name;
            this.code = code;
        }

        // 普通方法
        public static String getName(Integer code) {
            for (Enum_CommonStatus c : Enum_CommonStatus.values()) {
                if (code == c.getCode()) {
                    return c.getName();
                }
            }
            return null;
        }

        // 普通方法
        public static Integer getCode(String name) {
            for (Enum_CommonStatus c : Enum_CommonStatus.values()) {
                if (name.equals(c.getName())) {
                    return c.getCode();
                }
            }
            return null;
        }

        // get set 方法
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }
    }


}
