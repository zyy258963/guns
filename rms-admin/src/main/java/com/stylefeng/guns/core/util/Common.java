package com.stylefeng.guns.core.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.stylefeng.guns.common.persistence.model.OrganData;
import com.stylefeng.guns.common.persistence.model.ReportData;
import com.stylefeng.guns.common.persistence.model.ReportTemplate;
import com.stylefeng.guns.modular.report.dao.OrganDataMapper;
import com.stylefeng.guns.modular.report.dao.ReportDataMapper;
import com.stylefeng.guns.modular.report.dao.ReportTemplateMapper;
import com.stylefeng.guns.modular.report.service.impl.ReportDataServiceImpl;
import com.stylefeng.guns.modular.report.task.TransferHtml2PdfTask;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.DocumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static org.aspectj.weaver.tools.cache.SimpleCacheFactory.path;

@Component
public class Common {
    private static final Logger LOGGER = LoggerFactory.getLogger(Common.class);

    @Value("${report.descFilePath}")
    private String descFilePath;
    @Value("${report.scanFilePath}")
    private String scanFilePath;
    @Value("${server.domain}")
    private String domain;
    @Value("${server.port}")
    private String port;
    @Value("${server.context}")
    private String context;
    @Autowired
    private ReportDataServiceImpl reportDataService;

    private String filenameTemp = null;

    @Autowired
    private ReportTemplateMapper reportTemplateMapper;

    @Autowired
    private OrganDataMapper organDataMapper;
    /**
     * 判断是传入的参数是否为空
     *
     */
    public Boolean CheckReportTypeAndOrganCode(JSONObject job){
        // 判断
        if("".equals(job.get("reportType"))||job.get("reportType")== null){

            return false;
        }
        if("".equals(job.get("organCode"))||job.get("organCode")== null) {

            return false;
        }
        if("".equals(job.get("channelType"))||job.get("channelType")== null) {

            return false;
        }
        return true;
    }

    /**
     * 单个pdf下载
     *
     */
    public void downloadFile(String fullPath,String name,HttpServletResponse response){
        // 获得真实的路径
        // 获得 文件流并下载
        // Get your file stream from wherever.
        File downloadFile = new File(fullPath);
        response.setContentType("application/force-download");// 设置强制下载不打开
        response.setHeader("Content-Disposition", "attachment;fileName=" + name);// 设置文件名
        byte[] buffer = new byte[1024];
        FileInputStream fis = null;
        BufferedInputStream bis = null;
        try {
            fis = new FileInputStream(downloadFile);
            bis = new BufferedInputStream(fis);
            DataOutputStream os = new DataOutputStream (response.getOutputStream());
            int i = bis.read(buffer);
            while (i != -1) {
                os.write(buffer, 0, i);
                i = bis.read(buffer);
            }
            os.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 压缩包下载
     *
     */
    public static boolean downloadFileZip(String filePath,HttpServletResponse response) {
        try {
            String downloadFilename = "document.zip";//文件的名称
            downloadFilename = URLEncoder.encode(downloadFilename, "UTF-8");//转换中文否则可能会产生乱码
            response.setContentType("application/octet-stream");// 指明response的返回对象是文件流
            response.setHeader("Content-Disposition", "attachment;fileName=" + downloadFilename);// 设置在下载框默认显示的文件名
            ZipOutputStream zos = new ZipOutputStream(response.getOutputStream());
            File file= new File(filePath);
            File[] tempList = file.listFiles();
            FileInputStream in = null;
            for (int i = 0; i < tempList.length; i++) {
                if (tempList[i].isFile()) {
                    zos.putNextEntry(new ZipEntry(tempList[i].getName()));
                    in = new FileInputStream(tempList[i]);
                    byte[] buffer = new byte[4096];
                    int count = 0;
                    while ((count = in.read(buffer)) > 0) {
                        zos.write(buffer, 0, count);
                    }
                }
                if (tempList[i].isDirectory()) {
                }
            }
            in.close();
            zos.close();
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    /**
     * 返回json数据
     *
     */
    public static boolean returnJson(JSONObject jsonObject,HttpServletResponse response) {

        response.setContentType("application/octet-stream");// 指明response的返回对象是文件流
            try {
                DataOutputStream os = new DataOutputStream(response.getOutputStream());
                os.write(jsonObject.toString().getBytes("UTF-8"));
                os.flush();
                os.close();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
    }
    /**
     * 把xml文件转换为PDF
     *
     * @param jsonEntitys  Json数组
     * @param response
     */
    public Object xmlToPdf(JSONArray jsonEntitys,HttpServletResponse response,String flag){
        // 接收 json参数
        Boolean messageFlag = null;
        // 生成文件路径
        // 时间戳
        long date =  new Date().getTime();
        // 随机数
        int num = (int)(Math.random()*(9999-1000+1))+1000;
        String filePath = descFilePath + File.separator + DateUtil.getDays()+ File.separator + date+ num;
        // 单个pdf文件名
        String singleName = null;
        // 获取信函编号
        String cardNo = null;
        // 转pdf flag
        Boolean result = false;
        // 判断打印全局是否有打印失败的情况
        Boolean printsResult = false;
        // 单个打印结果是否成功
        Boolean printResult = true;
        // 创建json数组
        JSONArray jsonArray = new JSONArray();
        // 创建json用于打印返回
        JSONObject jsonObjectForReturn = new JSONObject();
        // 打印机机构是否为空
        boolean isOrganEmpty = false;

        File file =new File(filePath);
        //如果文件夹不存在则创建
        if  (!file .exists()  && !file .isDirectory()) {
            file .mkdir();
        }
        String path = filePath + File.separator;
        // 批量的场合
        if (jsonEntitys.size() > 1) {
            // 预览和预览打印，创建PrintFail.txt文件
            if ("1".equals(flag) || "3".equals(flag)) {
                try {
                    creatTxtFile(path);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        //  循环解析json数组
        for(int i=0;i<jsonEntitys.size();i++){
            JSONObject ja = new JSONObject();
            JSONObject job = jsonEntitys.getJSONObject(i); // 遍历 jsonarray 数组，把每一个对象转成 json 对象
            System.out.println(job.getString("reportType")+"---"+job.getString("organCode")+"---"+job.getString("channelType"));
            messageFlag = CheckReportTypeAndOrganCode(job);
            // 获取xml字符串
            String xmlStr = job.getString("reportData");
            // 生成 pdf
            JSONObject js = null;
            try {
                // 生成pdf文件
                js = transferXmlFile2Pdf(xmlStr, job.getString("reportType"), job.getString("channelType"), filePath,messageFlag,job.getString("organCode"));
                // 获取文件名
                singleName = js.getString("fileName");
                // 获取转换pdf结果
                result = js.getBoolean("result");
                // 获取信函编号
                cardNo = js.getString("cardNo");
                OrganData ogranData = null;
                // 判断打印和打印预览的情况开始打印
                if ("2".equals(flag) || "3".equals(flag)) {
                    // 获取分公司打印机名称
                    if (messageFlag){
                        OrganData ogranParam = new OrganData();
                        ogranParam.setOrganCode(job.getString("organCode"));
                        ogranData = organDataMapper.selectOne(ogranParam);
                        // 如果8位打印机构不存在，则取6位
                        if (ogranData ==null && job.getString("organCode").length() == 8) {
                            OrganData ogranParam1 = new OrganData();
                            ogranParam1.setOrganCode(job.getString("organCode").substring(0,6));
                            ogranData = organDataMapper.selectOne(ogranParam1);
                        }
                        // 如果打印机构不存在，本次请求失败
                        if (ogranData ==null ||ogranData.getPrintCode()==null ||"".equals(ogranData.getPrintCode())){
                            printResult = false;
                            messageFlag = false;
                            result = false;
                            isOrganEmpty = true;
                        }
                    }
                    // 转换pdf失败
                    if (!result){
                        printResult = false;
                    }
                    // 打印
                    if(result && messageFlag){
                        String cmdStr = "lp -d "+ogranData.getPrintCode() + " " + filePath + File.separator + singleName;
                        try {
                            String rs = CommandLineUtil.exeCmd(cmdStr);
                            printsResult = true ;
                        } catch (Exception e) {
                            e.printStackTrace();
                            printResult = false;
                        }
                    }
                }
                // 生成单个pdf
                if (jsonEntitys.size() == 1){

                    // 打印成功的情况
                    if (messageFlag  && result && printResult){
                        ja.put("cardNo",cardNo);
                        ja.put("message","打印成功");
                    } else {
                        printsResult = false;
                        ja.put("cardNo",cardNo);
                        ja.put("message","打印失败");
                    }
                    jsonArray.add(ja);
                } else { // 批量的情况
                    // 打印的场合
                    if ("2".equals(flag)){
                        if (!messageFlag || !result || !printResult){
                            // 批量打印只要一次打印错误，即设置为false
                            printsResult = false;
                            ja.put("cardNo",cardNo);
                            ja.put("message","打印失败");
                            jsonArray.add(ja);
                        }
                    } else { // 非打印场合
                        if (!messageFlag || !result || !printResult){
                            try {
                                writeTxtFile(singleName+"下载或打印失败。");
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                if ("2".equals(flag)){
                    ja.put("cardNo","数据解析异常");
                    ja.put("message","打印失败");
                } else if("3".equals(flag)){
                    try {
                        writeTxtFile("数据异常下载或打印失败。");
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                } else {
                    result = false;
                }
                continue;
            }
        }
        // 判断只有是预览或者打印预览的时候，才可以下载文件
        if ("1".equals(flag) || "3".equals(flag)) {
            if (jsonEntitys.size() == 1) {
                // 生成单个pdf，直接下载
                // 传入参数不为空，pdf成功生成的情况
                if ((messageFlag && result) || isOrganEmpty){
                    downloadFile(filePath + File.separator + singleName, singleName, response);
                }else{
                    downloadFile("/home/report/failpdf"+File.separator+"fail.pdf", "fail.pdf", response);
                }
            } else if (jsonEntitys.size() > 1) {
                // 把多个打成压缩包返回
                downloadFileZip(filePath, response);
            }
        } else {
            // 判断是否有打印错误的情况
            if (printsResult) {
                jsonObjectForReturn.put("success",true);
            } else {
                jsonObjectForReturn.put("success",false);
            }
            jsonObjectForReturn.put("info",jsonArray);
            returnJson(jsonObjectForReturn,response);
        }
        return "";
    }

    /**
     * 把xml文件转换为PDF
     *
     * @param xmlFile  xml串
     * @param reportType 信函类型
     * @param channelType 渠道编号
     * @param filePath1 生成文档路径
     * @param messageFlag 传入参数有空值
     */
    private JSONObject transferXmlFile2Pdf(String xmlFile,String reportType,String channelType,String filePath1,boolean messageFlag,String organCode) throws DocumentException {
        /* 解析 xml 文件 */
        // html->pdf 是否成功
        boolean result = true;
        JSONObject json = null;
        json = XmlToJsonUtil.transXml2Json(xmlFile);
        //如果存在信函类型,则json数据全部存取全部,不存在，取LETTER_DETAIL节点
        JSONObject reportHeaderJson = null;
        JSONObject reportBodyJson = null;
        // 判断如果LETTER_ID 存在的情况下，json数据不需要取LETTER_DETAILS节点
        if (json.getString("LETTER_ID")==null || "".equals(json.getString("LETTER_ID"))) {
             reportHeaderJson = json.getJSONObject("LETTER_DETAILS");
             reportBodyJson = reportHeaderJson.getJSONObject("LETTER_DETAIL");
        } else {
            reportBodyJson = json;
        }
        ReportData reportData = saveReportData(reportBodyJson,channelType,xmlFile,reportType,organCode);
        String fileName = null;
        // 返回错误信息中的cardNO
        // 可能为信号编号，理赔号，保单号
        String cardNo = null;
        // 构建生成的文件路径
        // 判断信号编号是否存在，存在则用信函编号，不存在判断保单号
        if (reportBodyJson.getString("CARD_NO")==null || "".equals(reportBodyJson.getString("CARD_NO"))) {
            // 判断保单号是否存在，存在则用保单号，不存在判断理赔号
            if(reportBodyJson.getString("POLICY_CODE")==null || "".equals(reportBodyJson.getString("POLICY_CODE"))){
                // 判断理赔号是否存在，存在则用理赔号，不存在判断保单号
                if (reportBodyJson.getString("CASE_NO") ==null || "".equals(reportBodyJson.getString("CASE_NO"))){
                    fileName = reportBodyJson.getString("POLICY_NO") + "_" + channelType + ".pdf";
                    cardNo = reportBodyJson.getString("POLICY_NO");
                } else {
                    fileName = reportBodyJson.getString("CASE_NO") + "_" + channelType + ".pdf";
                    cardNo = reportBodyJson.getString("CASE_NO");
                }
            } else {
                fileName = reportBodyJson.getString("POLICY_CODE") + "_" + channelType + ".pdf";
                cardNo =  reportBodyJson.getString("POLICY_CODE");
            }
        } else {
            fileName = reportBodyJson.getString("CARD_NO") +"_" +channelType + ".pdf";
            cardNo =  reportBodyJson.getString("CARD_NO");
        }
        String filePath = filePath1 + File.separator + fileName;
        // 判断信函类型是否存在
        ReportTemplate reportParam = new ReportTemplate();
        if("1043".equals(reportType)){
            reportParam.setCode(reportType);
            reportParam.setServiceid(reportBodyJson.getString("SERVICE_ID"));
        } else {
            reportParam.setCode(reportType);
        }
        ReportTemplate reportTemplate = reportTemplateMapper.selectOne(reportParam);
        if (reportTemplate == null || "".equals(reportTemplate.getPath())){
            result = false;
        } else {
            result =  HtmlToPdfUtil.convert(getServerAddress() + "/report/show?reportType=" + reportType + "&dataId=" + reportData.getId(), filePath);
        }
        // 更新 reportDate 的数据
        reportData.setFilePath(fileName);
        reportDataService.updateById(reportData);
        // 保存文件 和 路径到数据库
        LOGGER.info("生成文件成功 地址为： " + filePath);
        // 返回html->pdf 结果和fileName
        JSONObject js = new JSONObject();
        js.put("result",result);
        js.put("fileName",fileName);
        js.put("cardNo",cardNo);
        return js;
    }

    /**
     * 保存 信函数据信息
     *
     * @param reportBodyJson
     * @param channelType
     * @param xmlFile
     * @param reportType
     * @param organCode
     * @return
     */
    private ReportData saveReportData(JSONObject reportBodyJson,String channelType,String xmlFile,String reportType,String organCode) {
        // 调用 html 转 pdf 的程序
        ReportData reportData = new ReportData();
        reportData.setCode(reportType);
        reportData.setSendTime(reportBodyJson.getString("PRINT_DATE"));
        reportData.setCreateTime(new Date());
        reportData.setChannel(channelType);
        reportData.setOrgan(organCode);
        reportData.setXmldata(xmlFile);
        reportData.setData(reportBodyJson.toJSONString());
        reportDataService.insert(reportData);
        return reportData;
    }
    /**
     * 获得项目的访问地址
     *
     * @return
     */
    private String getServerAddress() {
        String tempUrl = "http://";
        if (StringUtils.isEmpty(domain)) {
            tempUrl += "localhost";
        } else if (domain.startsWith("http://")) {
            tempUrl += domain.replace("http://", "");
        } else {
            tempUrl += domain;
        }
        if (StringUtils.isEmpty(port)) {
            tempUrl += ":8080";
        } else if (port.startsWith(":")) {
            tempUrl += port;
        } else {
            tempUrl += ":" + port;
        }
        if (StringUtils.isNotEmpty(context)) {
            if (!"/".equalsIgnoreCase(context)) {
                if (context.startsWith("/")) {
                    tempUrl += context;
                }
            }
        }
        return tempUrl;
    }
    /**
     * 创建文件
     *
     * @throws IOException
     */
    public boolean creatTxtFile(String path) throws IOException {
        boolean flag = false;
        filenameTemp = path + "PrintFail" + ".txt";
        File filename = new File(filenameTemp);
        if (!filename.exists()) {
            filename.createNewFile();
            flag = true;
        }
        return flag;
    }

    /**
     * 写文件
     *
     * @param newStr
     *            新内容
     * @throws IOException
     */
    public boolean writeTxtFile(String newStr) throws IOException {
        // 先读取原有文件内容，然后进行写入操作
        boolean flag = false;
        String filein = newStr + "\r\n";
        String temp = "";

        FileInputStream fis = null;
        InputStreamReader isr = null;
        BufferedReader br = null;

        FileOutputStream fos = null;
        PrintWriter pw = null;
        try {
            // 文件路径
            File file = new File(filenameTemp);
            // 将文件读入输入流
            fis = new FileInputStream(file);
            isr = new InputStreamReader(fis);
            br = new BufferedReader(isr);
            StringBuffer buf = new StringBuffer();

            // 保存该文件原有的内容
            for (int j = 1; (temp = br.readLine()) != null; j++) {
                buf = buf.append(temp);
                // System.getProperty("line.separator")
                // 行与行之间的分隔符 相当于“\n”
                buf = buf.append(System.getProperty("line.separator"));
            }
            buf.append(filein);

            fos = new FileOutputStream(file);
            pw = new PrintWriter(fos);
            pw.write(buf.toString().toCharArray());
            pw.flush();
            flag = true;
        } catch (IOException e1) {
            // TODO 自动生成 catch 块
            throw e1;
        } finally {
            if (pw != null) {
                pw.close();
            }
            if (fos != null) {
                fos.close();
            }
            if (br != null) {
                br.close();
            }
            if (isr != null) {
                isr.close();
            }
            if (fis != null) {
                fis.close();
            }
        }
        return flag;
    }

}
