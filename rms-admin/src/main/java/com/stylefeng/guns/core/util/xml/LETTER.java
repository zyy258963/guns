package com.stylefeng.guns.core.util.xml;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.util.List;

@XStreamAlias("ROOT")
public class LETTER {


    @XStreamAlias("LETTER_DETAILS")
    private List<LETTER_DETAIL> letterDetails;

    public List<LETTER_DETAIL> getLetterDetails() {
        return letterDetails;
    }

    public void setLetterDetails(List<LETTER_DETAIL> letterDetails) {
        this.letterDetails = letterDetails;
    }

    @Override
    public String toString() {
        return "LETTER{" +
                "letterDetails=" + letterDetails +
                '}';
    }
}
