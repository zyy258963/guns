package com.stylefeng.guns.core.util.xml;

import com.thoughtworks.xstream.annotations.XStreamAlias;


@XStreamAlias("LETTER_DETAIL")
public class LETTER_DETAIL {
    @XStreamAlias("CARD_NO")
    private String CARD_NO;
    @XStreamAlias("PH_NAME")
    private String PH_NAME;
    @XStreamAlias("PH_TITLE")
    private String PH_TITLE;
    @XStreamAlias("CERTI_CODE")
    private String CERTI_CODE;
    @XStreamAlias("POLICY_CODE")
    private String POLICY_CODE;
    @XStreamAlias("CHANNEL_TYPE")
    private String CHANNEL_TYPE;
    @XStreamAlias("INSURED_NAME")
    private String INSURED_NAME;
    @XStreamAlias("INSURED_CERTI_CODE")
    private String INSURED_CERTI_CODE;
    @XStreamAlias("LIMIT_DESC")
    private String LIMIT_DESC;
    @XStreamAlias("BANK_AND_SAVING_AGENCY")
    private String BANK_AND_SAVING_AGENCY;
    @XStreamAlias("BUSINESS_GROUP")
    private String BUSINESS_GROUP;
    @XStreamAlias("MARKETING_GROUP")
    private String MARKETING_GROUP;
    @XStreamAlias("AGENT_NAME")
    private String AGENT_NAME;
    @XStreamAlias("AGENT_CODE")
    private String AGENT_CODE;
    @XStreamAlias("PRINT_ORGAN")
    private String PRINT_ORGAN;
    @XStreamAlias("PRINT_DATE")
    private String PRINT_DATE;

    public LETTER_DETAIL() {
    }

    public String getCARD_NO() {
        return CARD_NO;
    }

    public void setCARD_NO(String CARD_NO) {
        this.CARD_NO = CARD_NO;
    }

    public String getPH_NAME() {
        return PH_NAME;
    }

    public void setPH_NAME(String PH_NAME) {
        this.PH_NAME = PH_NAME;
    }

    public String getPH_TITLE() {
        return PH_TITLE;
    }

    public void setPH_TITLE(String PH_TITLE) {
        this.PH_TITLE = PH_TITLE;
    }

    public String getCERTI_CODE() {
        return CERTI_CODE;
    }

    public void setCERTI_CODE(String CERTI_CODE) {
        this.CERTI_CODE = CERTI_CODE;
    }

    public String getPOLICY_CODE() {
        return POLICY_CODE;
    }

    public void setPOLICY_CODE(String POLICY_CODE) {
        this.POLICY_CODE = POLICY_CODE;
    }

    public String getCHANNEL_TYPE() {
        return CHANNEL_TYPE;
    }

    public void setCHANNEL_TYPE(String CHANNEL_TYPE) {
        this.CHANNEL_TYPE = CHANNEL_TYPE;
    }

    public String getINSURED_NAME() {
        return INSURED_NAME;
    }

    public void setINSURED_NAME(String INSURED_NAME) {
        this.INSURED_NAME = INSURED_NAME;
    }

    public String getINSURED_CERTI_CODE() {
        return INSURED_CERTI_CODE;
    }

    public void setINSURED_CERTI_CODE(String INSURED_CERTI_CODE) {
        this.INSURED_CERTI_CODE = INSURED_CERTI_CODE;
    }

    public String getLIMIT_DESC() {
        return LIMIT_DESC;
    }

    public void setLIMIT_DESC(String LIMIT_DESC) {
        this.LIMIT_DESC = LIMIT_DESC;
    }

    public String getBANK_AND_SAVING_AGENCY() {
        return BANK_AND_SAVING_AGENCY;
    }

    public void setBANK_AND_SAVING_AGENCY(String BANK_AND_SAVING_AGENCY) {
        this.BANK_AND_SAVING_AGENCY = BANK_AND_SAVING_AGENCY;
    }

    public String getBUSINESS_GROUP() {
        return BUSINESS_GROUP;
    }

    public void setBUSINESS_GROUP(String BUSINESS_GROUP) {
        this.BUSINESS_GROUP = BUSINESS_GROUP;
    }

    public String getMARKETING_GROUP() {
        return MARKETING_GROUP;
    }

    public void setMARKETING_GROUP(String MARKETING_GROUP) {
        this.MARKETING_GROUP = MARKETING_GROUP;
    }

    public String getAGENT_NAME() {
        return AGENT_NAME;
    }

    public void setAGENT_NAME(String AGENT_NAME) {
        this.AGENT_NAME = AGENT_NAME;
    }

    public String getAGENT_CODE() {
        return AGENT_CODE;
    }

    public void setAGENT_CODE(String AGENT_CODE) {
        this.AGENT_CODE = AGENT_CODE;
    }

    public String getPRINT_ORGAN() {
        return PRINT_ORGAN;
    }

    public void setPRINT_ORGAN(String PRINT_ORGAN) {
        this.PRINT_ORGAN = PRINT_ORGAN;
    }

    public String getPRINT_DATE() {
        return PRINT_DATE;
    }

    public void setPRINT_DATE(String PRINT_DATE) {
        this.PRINT_DATE = PRINT_DATE;
    }

    @Override
    public String toString() {
        return "LETTER_DETAIL{" +
                "CARD_NO='" + CARD_NO + '\'' +
                ", PH_NAME='" + PH_NAME + '\'' +
                ", PH_TITLE='" + PH_TITLE + '\'' +
                ", CERTI_CODE='" + CERTI_CODE + '\'' +
                ", POLICY_CODE='" + POLICY_CODE + '\'' +
                ", CHANNEL_TYPE='" + CHANNEL_TYPE + '\'' +
                ", INSURED_NAME='" + INSURED_NAME + '\'' +
                ", INSURED_CERTI_CODE='" + INSURED_CERTI_CODE + '\'' +
                ", LIMIT_DESC='" + LIMIT_DESC + '\'' +
                ", BANK_AND_SAVING_AGENCY='" + BANK_AND_SAVING_AGENCY + '\'' +
                ", BUSINESS_GROUP='" + BUSINESS_GROUP + '\'' +
                ", MARKETING_GROUP='" + MARKETING_GROUP + '\'' +
                ", AGENT_NAME='" + AGENT_NAME + '\'' +
                ", AGENT_CODE='" + AGENT_CODE + '\'' +
                ", PRINT_ORGAN='" + PRINT_ORGAN + '\'' +
                ", PRINT_DATE='" + PRINT_DATE + '\'' +
                '}';
    }
}
