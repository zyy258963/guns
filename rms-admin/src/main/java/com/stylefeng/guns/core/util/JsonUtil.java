package com.stylefeng.guns.core.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class JsonUtil {

    /**
     * 判断字符串是否是 json 数组或者 json 对象
     *
     * @param str
     * @return
     */
    public static boolean isJsonArray(Object str) {
        try {
            Object json = new JSONTokener(str.toString()).nextValue();
            if (json instanceof JSONObject) {
                return false;
            } else if (json instanceof JSONArray) {
                return true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }
}
