package com.stylefeng.guns.core.util;


import com.alibaba.fastjson.JSONObject;

import javax.print.*;
import javax.print.attribute.DocAttributeSet;
import javax.print.attribute.HashDocAttributeSet;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import java.io.File;
import java.io.FileInputStream;

public class PrintUtil {


    public static void printFile(String path) throws Exception {
        File file = new File(path);
        File[] fies = file.listFiles();
        for (File f : fies) {
            System.out.println("file " + f.getName());
            String fileExt = f.getName().substring(f.getName().indexOf(".") + 1, f.getName().length());
            if ("pdf".equalsIgnoreCase(fileExt)) {
                String filepath = path + File.separator + f.getName();
                File pdfFile = new File(filepath);
                //构建打印请求属性集
                PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
                //设置打印格式，因为未确定文件类型，这里选择AUTOSENSE
                DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
                //查找所有的可用打印服务
                PrintService printService[] = PrintServiceLookup.lookupPrintServices(flavor, pras);
                for(PrintService p : printService){
                    System.out.println("打印机信息：" + p.getName() + "   " );
                }
                //定位默认的打印服务
                PrintService defaultService = PrintServiceLookup.lookupDefaultPrintService();
                System.out.println("默认打印机信息：" + defaultService.getName());
                //显示打印对话框
                //PrintService service = ServiceUI.printDialog(null, 200, 200, printService,   defaultService, flavor, pras);
//                if (defaultService != null) {
//                    DocPrintJob job = defaultService.createPrintJob(); //创建打印作业
//                    FileInputStream fis = new FileInputStream(pdfFile); //构造待打印的文件流
//                    DocAttributeSet das = new HashDocAttributeSet();
//                    Doc doc = new SimpleDoc(fis, flavor, das); //建立打印文件格式
//                    job.print(doc, pras); //进行文件的打印
//                }
//                f.delete();
            }
        }
    }

    public static void main(String[] args) {
        //System.out.println("Value:"+test());
        //打印pdf的一个方法，首先安装下PDFCreator软件

        try {
            printFile("/home/wwwroot" + File.separator + "1.pdf");
        } catch (Exception e) {
            System.out.println("打印文件异常：" + e.getMessage());
            e.printStackTrace();
        }

    }

}