package com.stylefeng.guns.core.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class CommandLineUtil {

    public static String exeCmd(String commandStr) throws Exception {
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        try {
            Process p = Runtime.getRuntime().exec(commandStr);
            br = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = null;
            while ((line = br.readLine()) != null) {
                sb.append(line + "\n");
            }
            System.out.println(sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        finally
        {
            if (br != null)
            {
                try {
                    br.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(args);
        if(args.length > 0){
            System.out.println(args[0]);
//            exeCmd(args[0]);
        }else{
            System.out.println("没有输入对应的参数");
        }
    }
}
