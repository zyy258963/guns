package com.stylefeng.guns.core.util.xml;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.basic.DateConverter;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;

import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.TimeZone;

public class Test {

    public static final XmlFriendlyNameCoder nameCoder = new XmlFriendlyNameCoder();

    // 编码格式
    private static final String ENCODING = "UTF-8";

    // dom解析驱动
    private static final DomDriver fixDriver = new DomDriver(ENCODING, nameCoder);

    // 通用解析器
    public static final XStream XSTREAM = new XStream(fixDriver);

    private static final String CHINA_TIME_ZONE = "Asia/Shanghai";

    static {
        // 时区处理
        TimeZone zone = TimeZone.getTimeZone(CHINA_TIME_ZONE);
        XSTREAM.registerConverter(new DateConverter(zone), XStream.PRIORITY_NORMAL);
        XSTREAM.autodetectAnnotations(true);

    }

    private Test() {
    }

    public static void main(String[] args) {
        try {
            // 让Root节点的XML与AReq对象关联
            XSTREAM.alias("ROOT", LETTER.class);

            // XML文件生成对象
            LETTER req3 = (LETTER) XSTREAM.fromXML(new File("C:\\Users\\Administrator\\Desktop\\1.xml"));
            System.out.println("req3=" + req3);
            System.out.println("-------------------------");
            // XStream还支持输入流、URL等方式生成对象。
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    public static String readFile(String path) throws Exception {
        File file = new File(path);
        FileInputStream fis = new FileInputStream(file);
        FileChannel fc = fis.getChannel();
        ByteBuffer bb = ByteBuffer.allocate(new Long(file.length()).intValue());
        //fc向buffer中读入数据
        fc.read(bb);
        bb.flip();
        String str = new String(bb.array(), "UTF8");
        fc.close();
        fis.close();
        return str;
    }
}
