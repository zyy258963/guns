package com.stylefeng.guns.common.persistence.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhangyanyan123
 * @since 2018-07-25
 */
@TableName("sino_organ_data")
public class OrganData extends Model<OrganData> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 打印机构编号
     */
    @TableField("organ_code")
    private String organCode;
    /**
     * 打印机构名称
     */
    @TableField("organ_name")
    private String organName;
    /**
     * 打印机型号
     */
    @TableField("print_model")
    private String printModel;
    /**
     * IP地址
     */
    private String ip;
    /**
     * 打印机号码，打印name
     */
    @TableField("print_code")
    private String printCode;
    /**
     * 描述
     */
    private String description;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrganCode() {
        return organCode;
    }

    public void setOrganCode(String organCode) {
        this.organCode = organCode;
    }

    public String getOrganName() {
        return organName;
    }

    public void setOrganName(String organName) {
        this.organName = organName;
    }

    public String getPrintModel() {
        return printModel;
    }

    public void setPrintModel(String printModel) {
        this.printModel = printModel;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPrintCode() {
        return printCode;
    }

    public void setPrintCode(String printCode) {
        this.printCode = printCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "OrganData{" +
        "id=" + id +
        ", organCode=" + organCode +
        ", organName=" + organName +
        ", printModel=" + printModel +
        ", ip=" + ip +
        ", printCode=" + printCode +
        ", description=" + description +
        "}";
    }
}
