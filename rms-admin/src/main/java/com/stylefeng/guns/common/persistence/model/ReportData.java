package com.stylefeng.guns.common.persistence.model;

import java.io.Serializable;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;


/**
 * <p>
 * 报告数据信息
 * </p>
 *
 * @author zhangyanyan123
 * @since 2018-01-26
 */
@TableName("sino_report_data")
public class ReportData extends Model<ReportData> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ID_WORKER)
    private Long id;
    /**
     * 对接渠道
     */
    private String channel;
    /**
     * 对接渠道
     */
    @TableField(exist = false)
    private String channelName;
    /**
     * 信函类型
     */
    private String code;
    /**
     * 对接渠道
     */
    @TableField(exist = false)
    private String codeName;
    /**
     * 发送时间
     */
    @TableField("send_time")
    private String sendTime;
    /**
     * 传输的数据
     */
    private String data;
    /**
     * 传输过来的数据
     */
    @TableField("xml_data")
    private String xmldata;
    /**
     * 打印机构
     */
    @TableField("organ")
    private String organ;
    /**
     * 生成文件路径
     */
    @TableField("file_path")
    private String filePath;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public String getOrgan() {
        return organ;
    }

    public void setOrgan(String organ) {
        this.organ = organ;
    }

    public String getXmldata() {
        return xmldata;
    }

    public void setXmldata(String xmldata) {
        this.xmldata = xmldata;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    @Override
    public String toString() {
        return "ReportData{" +
                "id=" + id +
                ", channel='" + channel + '\'' +
                ", channelName='" + channelName + '\'' +
                ", code='" + code + '\'' +
                ", codeName='" + codeName + '\'' +
                ", sendTime='" + sendTime + '\'' +
                ", data='" + data + '\'' +
                ", xmldata='" + xmldata + '\'' +
                ", organ='" + organ + '\'' +
                ", filePath='" + filePath + '\'' +
                ", createTime=" + createTime +
                '}';
    }
}
