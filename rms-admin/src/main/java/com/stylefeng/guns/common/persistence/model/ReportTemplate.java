package com.stylefeng.guns.common.persistence.model;

import java.io.Serializable;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 信函模版
 * </p>
 *
 * @author zhangyanyan123
 * @since 2018-01-25
 */
@TableName("sino_report_template")
public class ReportTemplate extends Model<ReportTemplate> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ID_WORKER)
    private Long id;
    /**
     * 模版名称
     */
    private String name;
    /**
     * 模版编码
     */
    private String code;
    /**
     * 模版路径
     */
    private String path;
    /**
     * 模版版本
     */
    private String version;
    /**
     * 业务模块
     */
    private String module;
    /**
     * 业务模块
     */
    @TableField(exist = false)
    private String moduleName;
    /**
     * 服务id
     */
    private String serviceid;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;
    /**
     * 创建人
     */
    private Integer operator;
    /**
     * 创建人
     */
    @TableField(exist = false)
    private String operatorName;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 状态
     */
    @TableField(exist = false)
    private String statusName;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getOperator() {
        return operator;
    }

    public void setOperator(Integer operator) {
        this.operator = operator;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getServiceid() {
        return serviceid;
    }

    public void setServiceid(String serviceid) {
        this.serviceid = serviceid;
    }

    @Override
    public String toString() {
        return "ReportTemplate{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", path='" + path + '\'' +
                ", version='" + version + '\'' +
                ", module='" + module + '\'' +
                ", moduleName='" + moduleName + '\'' +
                ", serviceid='" + serviceid + '\'' +
                ", createTime=" + createTime +
                ", operator=" + operator +
                ", operatorName='" + operatorName + '\'' +
                ", status=" + status +
                ", statusName='" + statusName + '\'' +
                '}';
    }
}
