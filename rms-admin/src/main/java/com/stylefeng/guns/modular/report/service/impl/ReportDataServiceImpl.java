package com.stylefeng.guns.modular.report.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.persistence.model.ReportData;
import com.stylefeng.guns.common.persistence.model.ReportTemplate;
import com.stylefeng.guns.core.support.BeanKit;
import com.stylefeng.guns.modular.report.dao.ReportDataMapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.report.dao.ReportTemplateMapper;
import com.stylefeng.guns.modular.report.service.IReportDataService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 报告数据信息 服务实现类
 * </p>
 *
 * @author zyy258963
 * @Date 2018-01-26 17:35:13
 */
@Service
public class ReportDataServiceImpl extends ServiceImpl<ReportDataMapper, ReportData> implements IReportDataService {
    @Resource
    ReportDataMapper reportDataMapper;
   /* @Resource
    OgranDataMapper ogranDataMapper;*/
    @Autowired
    private ReportTemplateMapper reportTemplateMapper;

     /**
     * 根据查询条件，查询当前表的列表
     * @param page
     * @param reportData
     * @return
     */
    public Page<ReportData> selectPageList(Page<ReportData> page, ReportData reportData) {
        page.setRecords(reportDataMapper.selectPageList(page, BeanKit.beanToMap(reportData)));
        return page;
    }
    /**
     * 根据查询条件，查询公司名
     * @param organCode
     * @return
     */
    public String  findByOrgan(String organCode){
            // 从数据库查找对应公司名
       /* OgranData ogranParam = new OgranData();
        ogranParam.setOgranCode(organCode);
        OgranData ogranData = ogranDataMapper.selectOne(ogranParam);*/
        return "";
    }

     /**
     * 根据查询条件，查询信函模板
     * @param reportType
     * @return
     */
    public String findNameByReportType(String reportType){
        if (StringUtils.isNotEmpty(reportType)) {
            ReportTemplate reportParam = new ReportTemplate();
            reportParam.setCode(reportType);
            ReportTemplate reportTemplate = reportTemplateMapper.selectOne(reportParam);
            return  reportTemplate.getName();
        } else {
            return "500";
        }
    }
}
