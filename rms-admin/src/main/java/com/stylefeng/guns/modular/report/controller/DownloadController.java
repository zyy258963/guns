package com.stylefeng.guns.modular.report.controller;

import com.alibaba.fastjson.JSONObject;
import com.stylefeng.guns.common.persistence.model.ReportData;
import com.stylefeng.guns.common.persistence.model.ReportTemplate;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.report.dao.ReportTemplateMapper;
import com.stylefeng.guns.modular.report.service.IReportDataService;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * 信函模版管理控制器
 *
 * @author fengshuonan
 * @Date 2018-01-24 17:55:29
 */
@Controller
@RequestMapping("/download")
public class DownloadController extends BaseController {


    @Value("${report.descFilePath}")
    private String descFilePath;


    @RequestMapping(value = "/reportData")
    public void reportData(){
        String filePath = getPara("filePath");
        // 获得真实的路径
        String fullPath  = descFilePath + File.separator + filePath;
        // 获得 文件流并下载
        // Get your file stream from wherever.
        File downloadFile = new File(fullPath);

        ServletContext context = getHttpServletRequest().getServletContext();

        // get MIME type of the file
        String mimeType = context.getMimeType(fullPath);
        if (mimeType == null) {
            // set to binary type if MIME mapping not found
            mimeType = "application/octet-stream";
            System.out.println("context getMimeType is null");
        }
        System.out.println("MIME type: " + mimeType);

        // set content attributes for the response
        getHttpServletResponse().setContentType(mimeType);
        getHttpServletResponse().setContentLength((int) downloadFile.length());

        // set headers for the response
        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"",
                downloadFile.getName());
        getHttpServletResponse().setHeader(headerKey, headerValue);

        // Copy the stream to the response's output stream.
        try {
            InputStream myStream = new FileInputStream(fullPath);
            IOUtils.copy(myStream,  getHttpServletResponse().getOutputStream());
            getHttpServletResponse().flushBuffer();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
    }


}
