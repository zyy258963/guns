/**
  * Copyright 2018 bejson.com 
  */
package com.stylefeng.guns.modular.report.template.pojo.hb;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Auto-generated: 2018-02-05 9:40:19
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@XmlRootElement(name="LIMIT_DESC")
public class LIMIT_DESC {

    private LIMIT_PRD_LIST LIMIT_PRD_LIST;
    private String LIMIT_PRD_SEQ_NO;

    public LIMIT_DESC() {
    }

    public void setLIMIT_PRD_LIST(LIMIT_PRD_LIST LIMIT_PRD_LIST) {
         this.LIMIT_PRD_LIST = LIMIT_PRD_LIST;
     }
     public LIMIT_PRD_LIST getLIMIT_PRD_LIST() {
         return LIMIT_PRD_LIST;
     }

    public void setLIMIT_PRD_SEQ_NO(String LIMIT_PRD_SEQ_NO) {
         this.LIMIT_PRD_SEQ_NO = LIMIT_PRD_SEQ_NO;
     }
     public String getLIMIT_PRD_SEQ_NO() {
         return LIMIT_PRD_SEQ_NO;
     }

}