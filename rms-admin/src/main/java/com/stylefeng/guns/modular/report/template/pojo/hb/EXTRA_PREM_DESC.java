/**
  * Copyright 2018 bejson.com 
  */
package com.stylefeng.guns.modular.report.template.pojo.hb;

/**
 * Auto-generated: 2018-02-05 9:40:19
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class EXTRA_PREM_DESC {

    private String EXTRA_PREM_DESC_NO;
    private PRODUCT_INFO PRODUCT_INFO;
    private String EXTRA_PREM_TOTAL_AMOUNT;
    private String PREM_TOTAL_AMOUNT;
    public void setEXTRA_PREM_DESC_NO(String EXTRA_PREM_DESC_NO) {
         this.EXTRA_PREM_DESC_NO = EXTRA_PREM_DESC_NO;
     }
     public String getEXTRA_PREM_DESC_NO() {
         return EXTRA_PREM_DESC_NO;
     }

    public void setPRODUCT_INFO(PRODUCT_INFO PRODUCT_INFO) {
         this.PRODUCT_INFO = PRODUCT_INFO;
     }
     public PRODUCT_INFO getPRODUCT_INFO() {
         return PRODUCT_INFO;
     }

    public void setEXTRA_PREM_TOTAL_AMOUNT(String EXTRA_PREM_TOTAL_AMOUNT) {
         this.EXTRA_PREM_TOTAL_AMOUNT = EXTRA_PREM_TOTAL_AMOUNT;
     }
     public String getEXTRA_PREM_TOTAL_AMOUNT() {
         return EXTRA_PREM_TOTAL_AMOUNT;
     }

    public void setPREM_TOTAL_AMOUNT(String PREM_TOTAL_AMOUNT) {
         this.PREM_TOTAL_AMOUNT = PREM_TOTAL_AMOUNT;
     }
     public String getPREM_TOTAL_AMOUNT() {
         return PREM_TOTAL_AMOUNT;
     }

}