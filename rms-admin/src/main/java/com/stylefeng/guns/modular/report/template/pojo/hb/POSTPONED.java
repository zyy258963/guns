/**
  * Copyright 2018 bejson.com 
  */
package com.stylefeng.guns.modular.report.template.pojo.hb;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Auto-generated: 2018-02-05 9:40:19
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@XmlRootElement(name="POSTPONED")
public class POSTPONED {

    private String POST_PRODUCT_NAME;
    private String POST_INTERNAL_ID;
    private String POST_SEQUEN;
    private String POST_REASON;
    public void setPOST_PRODUCT_NAME(String POST_PRODUCT_NAME) {
         this.POST_PRODUCT_NAME = POST_PRODUCT_NAME;
     }
     public String getPOST_PRODUCT_NAME() {
         return POST_PRODUCT_NAME;
     }

    public void setPOST_INTERNAL_ID(String POST_INTERNAL_ID) {
         this.POST_INTERNAL_ID = POST_INTERNAL_ID;
     }
     public String getPOST_INTERNAL_ID() {
         return POST_INTERNAL_ID;
     }

    public void setPOST_SEQUEN(String POST_SEQUEN) {
         this.POST_SEQUEN = POST_SEQUEN;
     }
     public String getPOST_SEQUEN() {
         return POST_SEQUEN;
     }

    public void setPOST_REASON(String POST_REASON) {
         this.POST_REASON = POST_REASON;
     }
     public String getPOST_REASON() {
         return POST_REASON;
     }

}