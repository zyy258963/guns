/**
  * Copyright 2018 bejson.com 
  */
package com.stylefeng.guns.modular.report.template.pojo.hb;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Auto-generated: 2018-02-05 9:40:19
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@XmlRootElement(name="DECLINED_POSTPINED")
public class DECLINED_POSTPINED {

    private POSTPONED POSTPONED;
    private DECLINED DECLINED;
    private String SEQ_NO;

    public DECLINED_POSTPINED() {
    }

    public void setPOSTPONED(POSTPONED POSTPONED) {
         this.POSTPONED = POSTPONED;
     }
     public POSTPONED getPOSTPONED() {
         return POSTPONED;
     }

    public void setDECLINED(DECLINED DECLINED) {
         this.DECLINED = DECLINED;
     }
     public DECLINED getDECLINED() {
         return DECLINED;
     }

    public void setSEQ_NO(String SEQ_NO) {
         this.SEQ_NO = SEQ_NO;
     }
     public String getSEQ_NO() {
         return SEQ_NO;
     }

}