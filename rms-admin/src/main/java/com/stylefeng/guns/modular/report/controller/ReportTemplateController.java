package com.stylefeng.guns.modular.report.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.shiro.ShiroKit;
import com.stylefeng.guns.core.util.ConstantEnum;
import com.stylefeng.guns.core.util.Convert;
import com.stylefeng.guns.core.util.DateUtil;
import com.stylefeng.guns.modular.report.dao.ReportTemplateMapper;
import com.stylefeng.guns.modular.report.service.IReportTemplateService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.common.persistence.model.ReportTemplate;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 信函模版管理控制器
 *
 * @author zyy258963
 * @Date 2018-01-25 18:01:53
 */
@Controller
@RequestMapping("/reportTemplate")
public class ReportTemplateController extends BaseController {

    private String PREFIX = "/report/reportTemplate/";

    @Autowired
    private IReportTemplateService reportTemplateService;

    @Autowired
    private ReportTemplateMapper reportTemplateMapper;

    /**
     * 跳转到信函模版管理首页
     */
    @RequestMapping("")
    public String index(Model model) {
        addModuleNameSelect(model);
        addStatusNameSelect(model);
        return PREFIX + "reportTemplate.html";
    }

    /**
     * 添加客户 的下拉数值
     *
     * @param model
     */
    protected void addModuleNameSelect(Model model) {
        Integer dictId = ConstantFactory.me().getDictIdByName(ConstantEnum.Enum_DictType.业务模块.getCode());
        model.addAttribute("moduleList", ConstantFactory.me().findInDict(dictId));
    }

    /**
     * 添加客户 的下拉数值
     *
     * @param model
     */
    protected void addStatusNameSelect(Model model) {
        Integer dictId = ConstantFactory.me().getDictIdByName(ConstantEnum.Enum_DictType.状态.getCode());
        model.addAttribute("statusList", ConstantFactory.me().findInDict(dictId));
    }

    /**
     * 跳转到添加信函模版管理
     */
    @RequestMapping("/reportTemplate_add")
    public String reportTemplateAdd(Model model) {
        addModuleNameSelect(model);
        addStatusNameSelect(model);
        return PREFIX + "reportTemplate_add.html";
    }

    /**
     * 跳转到修改信函模版管理
     */
    @RequestMapping("/reportTemplate_update/{reportTemplateId}")
    public String reportTemplateUpdate(@PathVariable Long reportTemplateId, Model model) {
        ReportTemplate reportTemplate = reportTemplateService.selectById(reportTemplateId);
        model.addAttribute("item", reportTemplate);
        LogObjectHolder.me().set(reportTemplate);
        addModuleNameSelect(model);
        addStatusNameSelect(model);
        return PREFIX + "reportTemplate_edit.html";
    }

    /**
     * 获取信函模版管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(ReportTemplate reportTemplate) {
        Page<ReportTemplate> page = new PageFactory<ReportTemplate>().defaultPage();
        page = reportTemplateService.selectPageList(page, reportTemplate);
        List<ReportTemplate> list = page.getRecords();
        for (ReportTemplate r : list) {
            r.setStatusName(ConstantEnum.Enum_CommonStatus.getName(r.getStatus()));
        }
        return super.packForBT(page);
    }

    /**
     * 获取信函模版管理管理列表
     */
    @RequestMapping(value = "/listAll")
    @ResponseBody
    public Object listAll() {
        List<Map<String, Object>> list = reportTemplateMapper.selectNoPageList(getRequestMapParams());
        for (Map<String, Object> r : list) {
            r.put("statusName", (ConstantEnum.Enum_CommonStatus.getName(Convert.toInt(r.get("status")))));
            r.put("moduleName", (ConstantFactory.me().getDictsByName(ConstantEnum.Enum_DictType.业务模块.getCode(), Convert.toInt(r.get("module")))));
        }
        return list;
    }


    /**
     * 获取信函模版管理管理列表
     */
//    @GetMapping(value = "/export")
//    public Object export() {
//        List<Map<String, Object>> list = reportTemplateMapper.selectNoPageListForExcel(getRequestMapParams());
//        String fileName = DateUtil.getAllTime() + " 信函模版管理列表.xlsx";
//        String filePath = exportToExcel(fileName, list);
//        return renderFile(fileName, filePath);
//    }

    /**
     * 新增信函模版管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(ReportTemplate reportTemplate) {
        reportTemplate.setCreateTime(new Date());
        reportTemplate.setOperator(ShiroKit.getUser().getId());
        reportTemplateService.insert(reportTemplate);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除信函模版管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Long reportTemplateId) {
        reportTemplateService.deleteById(reportTemplateId);
        return SUCCESS_TIP;
    }

    /**
     * 修改信函模版管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(ReportTemplate reportTemplate) {
        reportTemplate.setOperator(ShiroKit.getUser().getId());
        reportTemplateService.updateById(reportTemplate);
        return super.SUCCESS_TIP;
    }

    /**
     * 信函模版管理详情
     */
    @RequestMapping(value = "/detail/{reportTemplateId}")
    @ResponseBody
    public Object detail(@PathVariable("reportTemplateId") Long reportTemplateId) {
        return reportTemplateService.selectById(reportTemplateId);
    }
}
