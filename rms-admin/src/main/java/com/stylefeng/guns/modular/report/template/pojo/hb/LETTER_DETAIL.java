/**
  * Copyright 2018 bejson.com 
  */
package com.stylefeng.guns.modular.report.template.pojo.hb;

/**
 * Auto-generated: 2018-02-05 9:40:19
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class LETTER_DETAIL {

    private String CARD_NO;
    private String PH_NAME;
    private String PH_TITLE;
    private String CERTI_CODE;
    private String POLICY_CODE;
    private String CHANNEL_TYPE;
    private String INSURED_NAME;
    private String INSURED_CERTI_CODE;
    private EXTRA_PREM_DESC EXTRA_PREM_DESC;
    private SPECIAL_DESC SPECIAL_DESC;
    private LIMIT_DESC LIMIT_DESC;
    private DECLINED_POSTPINED DECLINED_POSTPINED;
    private String BUSINESS_GROUP;
    private String MARKETING_GROUP;
    private String AGENT_NAME;
    private String AGENT_CODE;
    private String PRINT_ORGAN;
    private String PRINT_DATE;
    public void setCARD_NO(String CARD_NO) {
         this.CARD_NO = CARD_NO;
     }
     public String getCARD_NO() {
         return CARD_NO;
     }

    public void setPH_NAME(String PH_NAME) {
         this.PH_NAME = PH_NAME;
     }
     public String getPH_NAME() {
         return PH_NAME;
     }

    public void setPH_TITLE(String PH_TITLE) {
         this.PH_TITLE = PH_TITLE;
     }
     public String getPH_TITLE() {
         return PH_TITLE;
     }

    public void setCERTI_CODE(String CERTI_CODE) {
         this.CERTI_CODE = CERTI_CODE;
     }
     public String getCERTI_CODE() {
         return CERTI_CODE;
     }

    public void setPOLICY_CODE(String POLICY_CODE) {
         this.POLICY_CODE = POLICY_CODE;
     }
     public String getPOLICY_CODE() {
         return POLICY_CODE;
     }

    public void setCHANNEL_TYPE(String CHANNEL_TYPE) {
         this.CHANNEL_TYPE = CHANNEL_TYPE;
     }
     public String getCHANNEL_TYPE() {
         return CHANNEL_TYPE;
     }

    public void setINSURED_NAME(String INSURED_NAME) {
         this.INSURED_NAME = INSURED_NAME;
     }
     public String getINSURED_NAME() {
         return INSURED_NAME;
     }

    public void setINSURED_CERTI_CODE(String INSURED_CERTI_CODE) {
         this.INSURED_CERTI_CODE = INSURED_CERTI_CODE;
     }
     public String getINSURED_CERTI_CODE() {
         return INSURED_CERTI_CODE;
     }

    public void setEXTRA_PREM_DESC(EXTRA_PREM_DESC EXTRA_PREM_DESC) {
         this.EXTRA_PREM_DESC = EXTRA_PREM_DESC;
     }
     public EXTRA_PREM_DESC getEXTRA_PREM_DESC() {
         return EXTRA_PREM_DESC;
     }

    public void setSPECIAL_DESC(SPECIAL_DESC SPECIAL_DESC) {
         this.SPECIAL_DESC = SPECIAL_DESC;
     }
     public SPECIAL_DESC getSPECIAL_DESC() {
         return SPECIAL_DESC;
     }

    public void setLIMIT_DESC(LIMIT_DESC LIMIT_DESC) {
         this.LIMIT_DESC = LIMIT_DESC;
     }
     public LIMIT_DESC getLIMIT_DESC() {
         return LIMIT_DESC;
     }

    public void setDECLINED_POSTPINED(DECLINED_POSTPINED DECLINED_POSTPINED) {
         this.DECLINED_POSTPINED = DECLINED_POSTPINED;
     }
     public DECLINED_POSTPINED getDECLINED_POSTPINED() {
         return DECLINED_POSTPINED;
     }

    public void setBUSINESS_GROUP(String BUSINESS_GROUP) {
         this.BUSINESS_GROUP = BUSINESS_GROUP;
     }
     public String getBUSINESS_GROUP() {
         return BUSINESS_GROUP;
     }

    public void setMARKETING_GROUP(String MARKETING_GROUP) {
         this.MARKETING_GROUP = MARKETING_GROUP;
     }
     public String getMARKETING_GROUP() {
         return MARKETING_GROUP;
     }

    public void setAGENT_NAME(String AGENT_NAME) {
         this.AGENT_NAME = AGENT_NAME;
     }
     public String getAGENT_NAME() {
         return AGENT_NAME;
     }

    public void setAGENT_CODE(String AGENT_CODE) {
         this.AGENT_CODE = AGENT_CODE;
     }
     public String getAGENT_CODE() {
         return AGENT_CODE;
     }

    public void setPRINT_ORGAN(String PRINT_ORGAN) {
         this.PRINT_ORGAN = PRINT_ORGAN;
     }
     public String getPRINT_ORGAN() {
         return PRINT_ORGAN;
     }

    public void setPRINT_DATE(String PRINT_DATE) {
         this.PRINT_DATE = PRINT_DATE;
     }
     public String getPRINT_DATE() {
         return PRINT_DATE;
     }

}