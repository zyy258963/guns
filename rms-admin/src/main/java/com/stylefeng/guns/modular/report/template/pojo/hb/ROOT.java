/**
  * Copyright 2018 bejson.com 
  */
package com.stylefeng.guns.modular.report.template.pojo.hb;

/**
 * Auto-generated: 2018-02-05 9:40:19
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class ROOT {

    private LETTER_DETAILS LETTER_DETAILS;
    public void setLETTER_DETAILS(LETTER_DETAILS LETTER_DETAILS) {
         this.LETTER_DETAILS = LETTER_DETAILS;
     }
     public LETTER_DETAILS getLETTER_DETAILS() {
         return LETTER_DETAILS;
     }

}