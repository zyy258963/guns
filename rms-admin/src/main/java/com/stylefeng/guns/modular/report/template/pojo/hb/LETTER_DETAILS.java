/**
  * Copyright 2018 bejson.com 
  */
package com.stylefeng.guns.modular.report.template.pojo.hb;

/**
 * Auto-generated: 2018-02-05 9:40:19
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class LETTER_DETAILS {

    private LETTER_DETAIL LETTER_DETAIL;
    public void setLETTER_DETAIL(LETTER_DETAIL LETTER_DETAIL) {
         this.LETTER_DETAIL = LETTER_DETAIL;
     }
     public LETTER_DETAIL getLETTER_DETAIL() {
         return LETTER_DETAIL;
     }

}