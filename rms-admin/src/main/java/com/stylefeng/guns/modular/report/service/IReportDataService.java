package com.stylefeng.guns.modular.report.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.persistence.model.ReportData;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 报告数据信息 服务类
 * </p>
 *
 * @author zyy258963
 * @Date 2018-01-26 17:35:13
 */
public interface IReportDataService extends IService<ReportData> {
    /**
     * 根据查询条件，查询当前表的列表
     * @param page
     * @param reportData
     * @return
     */
    Page<ReportData> selectPageList(Page<ReportData> page, ReportData reportData);
}
