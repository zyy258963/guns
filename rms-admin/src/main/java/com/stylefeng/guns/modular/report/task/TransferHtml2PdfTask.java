package com.stylefeng.guns.modular.report.task;

import com.alibaba.fastjson.JSONObject;
import com.stylefeng.guns.common.persistence.model.ReportData;
import com.stylefeng.guns.core.util.DateUtil;
import com.stylefeng.guns.core.util.HtmlToPdfUtil;
import com.stylefeng.guns.core.util.XmlToJsonUtil;
import com.stylefeng.guns.modular.report.service.impl.ReportDataServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.dom4j.DocumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.aop.interceptor.SimpleAsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Date;
import java.util.concurrent.Executor;

// 正式环境关闭定时任务
//@Configuration
//@Component // 此注解必加
//@EnableScheduling // 此注解必加
public class TransferHtml2PdfTask implements SchedulingConfigurer, AsyncConfigurer {
    private static final Logger LOGGER = LoggerFactory.getLogger(TransferHtml2PdfTask.class);


    @Value("${report.scanFilePath}")
    private String scanFilePath;

    @Value("${report.descFilePath}")
    private String descFilePath;

    @Value("${server.domain}")
    private String domain;
    @Value("${server.port}")
    private String port;
    @Value("${server.context}")
    private String context;

    @Autowired
    private ReportDataServiceImpl reportDataService;

    /*
    * 并行任务
    */
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        TaskScheduler taskScheduler = taskScheduler();
        taskRegistrar.setTaskScheduler(taskScheduler);
    }

    /**
     * 并行任务使用策略：多线程处理
     *
     * @return ThreadPoolTaskScheduler 线程池
     */
//    @Bean(destroyMethod = "shutdown")
    public ThreadPoolTaskScheduler taskScheduler() {
        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setPoolSize(20);
        scheduler.setThreadNamePrefix("task----");
        scheduler.setAwaitTerminationSeconds(60);
        scheduler.setWaitForTasksToCompleteOnShutdown(true);
        return scheduler;
    }

    /*
    * 异步任务
    */
    public Executor getAsyncExecutor() {
        Executor executor = taskScheduler();
        return executor;
    }

    /*
    * 异步任务 异常处理
    */
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return new SimpleAsyncUncaughtExceptionHandler();
    }

    // 正式环境关闭定时任务
//    @Scheduled(cron = "0/30 * * * * *")
    public void transferHtml2Pdf() {

        System.out.println("开始执行定时任务：" + System.currentTimeMillis());
        String organCode = "";
        // 从 FTP 获得 xml 文件
        File file = new File(scanFilePath);
        if (file.isDirectory()) {
            String[] xmlList = file.list();
            for (String xmlFile : xmlList) {
                System.out.println(xmlFile);
                transferXmlFile2Pdf("1013", organCode, file.getAbsolutePath() + File.separator +
                        xmlFile);
            }
        }
    }

    public void transferHtml2Pdf(String reportType) {
        String organCode = "";
        // 从 FTP 获得 xml 文件
        File file = new File(scanFilePath);
        if (file.isDirectory()) {
            String[] xmlList = file.list();
            for (String xmlFile : xmlList) {
                System.out.println(xmlFile);
                transferXmlFile2Pdf(reportType, organCode, file.getAbsolutePath() + File.separator + xmlFile);
            }
        }
    }

    /**
     * 把xml文件转换为PDF
     *
     * @param xmlFile
     */
    private void transferXmlFile2Pdf(String reportType, String organCode, String xmlFile) {
        // 解析 xml 文件
        JSONObject json = null;
        try {
            json = XmlToJsonUtil.transXmlFile2Json(xmlFile);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        JSONObject reportDetailsJson = json.getJSONObject("LETTER_DETAILS");
        JSONObject reportDetailJson = reportDetailsJson.getJSONObject("LETTER_DETAIL");
        ReportData reportData = saveReportData(reportType, organCode, reportDetailJson);

        // 构建生成的文件路径
        String fileName = reportDataService.findNameByReportType(reportType) + DateUtil.getAllTime() + ".pdf";
        HtmlToPdfUtil.convert(getServerAddress() + "/report/show?reportType=" + reportType + "&dataId=" + reportData.getId(), descFilePath + File.separator + fileName);

        // 更新 reportDate 的数据
        reportData.setFilePath(fileName);
        reportDataService.updateById(reportData);
        // 保存文件 和 路径到数据库
        LOGGER.info("生成文件成功 地址为： " + descFilePath + File.separator + fileName);
    }

    /**
     * 获得项目的访问地址
     *
     * @return
     */
    private String getServerAddress() {
        String tempUrl = "http://";
        if (StringUtils.isEmpty(domain)) {
            tempUrl += "localhost";
        } else if (domain.startsWith("http://")) {
            tempUrl += domain.replace("http://", "");
        } else {
            tempUrl += domain;
        }
        if (StringUtils.isEmpty(port)) {
            tempUrl += ":8080";
        } else if (port.startsWith(":")) {
            tempUrl += port;
        } else {
            tempUrl += ":" + port;
        }
        if (StringUtils.isNotEmpty(context)) {
            if (!"/".equalsIgnoreCase(context)) {
                if (context.startsWith("/")) {
                    tempUrl += context;
                }
            }
        }
        return tempUrl;
    }

    /**
     * 保存 信函数据信息
     *
     * @param reportType
     * @param reportBodyJson
     * @return
     */
    private ReportData saveReportData(String reportType, String organCode, JSONObject reportBodyJson) {
        // 调用 html 转 pdf 的程序
        ReportData reportData = new ReportData();
        reportData.setCode(reportBodyJson.getString("POLICY_TYPE"));
        reportData.setSendTime(reportBodyJson.getString("PRINT_DATE"));
        reportData.setCreateTime(new Date());
        reportData.setChannel(reportBodyJson.getString("POLICY_CODE"));
        reportData.setOrgan(organCode);
        reportData.setData(reportBodyJson.toJSONString());
        reportDataService.insert(reportData);
        return reportData;
    }

    /*public static void main(String[] args) {
        new TransferHtml2PdfTask().transferHtml2Pdf();
    }*/
}
