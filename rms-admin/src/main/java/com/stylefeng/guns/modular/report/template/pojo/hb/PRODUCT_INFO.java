/**
  * Copyright 2018 bejson.com 
  */
package com.stylefeng.guns.modular.report.template.pojo.hb;

/**
 * Auto-generated: 2018-02-05 9:40:19
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class PRODUCT_INFO {

    private EXTRA_CHILD EXTRA_CHILD;
    private String PRODUCT_SEQ_NO;
    private String EXTRA_PREM_START_DATE;
    private String EXTRA_PREM_END_DATE;
    private String PREM_REASON;
    private String ISU;
    private String PRODUCT_NAME;
    private String INTERNAL_CODE;
    public void setEXTRA_CHILD(EXTRA_CHILD EXTRA_CHILD) {
         this.EXTRA_CHILD = EXTRA_CHILD;
     }
     public EXTRA_CHILD getEXTRA_CHILD() {
         return EXTRA_CHILD;
     }

    public void setPRODUCT_SEQ_NO(String PRODUCT_SEQ_NO) {
         this.PRODUCT_SEQ_NO = PRODUCT_SEQ_NO;
     }
     public String getPRODUCT_SEQ_NO() {
         return PRODUCT_SEQ_NO;
     }

    public void setEXTRA_PREM_START_DATE(String EXTRA_PREM_START_DATE) {
         this.EXTRA_PREM_START_DATE = EXTRA_PREM_START_DATE;
     }
     public String getEXTRA_PREM_START_DATE() {
         return EXTRA_PREM_START_DATE;
     }

    public void setEXTRA_PREM_END_DATE(String EXTRA_PREM_END_DATE) {
         this.EXTRA_PREM_END_DATE = EXTRA_PREM_END_DATE;
     }
     public String getEXTRA_PREM_END_DATE() {
         return EXTRA_PREM_END_DATE;
     }

    public void setPREM_REASON(String PREM_REASON) {
         this.PREM_REASON = PREM_REASON;
     }
     public String getPREM_REASON() {
         return PREM_REASON;
     }

    public void setISU(String ISU) {
         this.ISU = ISU;
     }
     public String getISU() {
         return ISU;
     }

    public void setPRODUCT_NAME(String PRODUCT_NAME) {
         this.PRODUCT_NAME = PRODUCT_NAME;
     }
     public String getPRODUCT_NAME() {
         return PRODUCT_NAME;
     }

    public void setINTERNAL_CODE(String INTERNAL_CODE) {
         this.INTERNAL_CODE = INTERNAL_CODE;
     }
     public String getINTERNAL_CODE() {
         return INTERNAL_CODE;
     }

}