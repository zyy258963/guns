package com.stylefeng.guns.modular.report.dao;

import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.stylefeng.guns.common.persistence.model.ReportTemplate;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 信函模版 Mapper 接口
 * </p>
 *
 * @author zhangyanyan123
 * @since 2018-01-25
 */
public interface ReportTemplateMapper extends BaseMapper<ReportTemplate> {
    /**
     * 根据条件查询列表
     * @param page
     * @param params
     * @return
     */
    List<ReportTemplate> selectPageList(Pagination page, Map<String,Object> params);

    /**
     * 根据条件查询列表,不分页
     * @param params
     * @return
     */
    List<Map<String,Object>> selectNoPageList(Map<String,Object> params);

    /**
     * 根据条件查询列表,  为了导出excel
     * @param params
     * @return
     */
    List<Map<String,Object>> selectNoPageListForExcel(Map<String,Object> params);


    /**
     * 根据条件查询列表,  为了下拉列表
     * @return
     */
    List<Map<String,Object>> selectForSelect();
}
