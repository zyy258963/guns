package com.stylefeng.guns.modular.report.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.report.dao.ReportTemplateMapper;
import com.stylefeng.guns.common.persistence.model.ReportTemplate;
import com.stylefeng.guns.core.support.BeanKit;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.report.service.IReportTemplateService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 信函模版 服务实现类
 * </p>
 *
 * @author zyy258963
 * @Date 2018-01-25 18:01:53
 */
@Service
public class ReportTemplateServiceImpl extends ServiceImpl<ReportTemplateMapper, ReportTemplate> implements IReportTemplateService {

    @Resource
    ReportTemplateMapper reportTemplateMapper;

    /**
     * 根据查询条件，查询当前表的列表
     *
     * @param page
     * @param reportTemplate
     * @return
     */
    public Page<ReportTemplate> selectPageList(Page<ReportTemplate> page, ReportTemplate reportTemplate) {
        page.setRecords(reportTemplateMapper.selectPageList(page, BeanKit.beanToMap(reportTemplate)));
        return page;
    }
}
