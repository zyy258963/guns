package com.stylefeng.guns.modular.report.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.base.tips.Tip;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.util.*;
import com.stylefeng.guns.modular.report.dao.ReportDataMapper;
import com.stylefeng.guns.modular.report.service.impl.ReportDataServiceImpl;
import com.stylefeng.guns.modular.report.task.TransferHtml2PdfTask;
import com.sun.org.apache.xerces.internal.impl.io.UTF8Reader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.json.JsonParser;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.common.persistence.model.ReportData;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

/**
 * 信函数据管理控制器
 *
 * @author zyy258963
 * @Date 2018-01-26 17:35:13
 */
@Controller
@RequestMapping("/reportData")
public class ReportDataController extends BaseController {

    private String PREFIX = "/report/reportData/";

    @Autowired
    private ReportDataServiceImpl reportDataService;

    @Autowired
    private ReportDataMapper reportDataMapper;

//    @Autowired
//    private TransferHtml2PdfTask transferHtml2PdfTask;

    @Value("${report.descFilePath}")
    private String descFilePath;

    /**
     * 跳转到信函数据管理首页
     */
    @RequestMapping("")
    public String index(Model model) {
        return PREFIX + "reportData.html";
    }

    /**
     * 跳转到添加信函数据管理
     */
    @RequestMapping("/reportData_add")
    public String reportDataAdd(Model model) {
        return PREFIX + "reportData_add.html";
    }

    /**
     * 跳转到修改信函数据管理
     */
    @RequestMapping("/reportData_update/{reportDataId}")
    public String reportDataUpdate(@PathVariable Long reportDataId, Model model) {
        ReportData reportData = reportDataService.selectById(reportDataId);
        model.addAttribute("item", reportData);
        LogObjectHolder.me().set(reportData);
        return PREFIX + "reportData_edit.html";
    }

    /**
     * 获取信函数据管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(ReportData reportData) {
        Page<ReportData> page = new PageFactory<ReportData>().defaultPage();
        page = reportDataService.selectPageList(page, reportData);
        List<ReportData> list = page.getRecords();
        for (ReportData r : list) {
            r.setChannelName((ConstantFactory.me().getDictsByName(ConstantEnum.Enum_DictType.信函渠道.getCode(), Convert.toInt(r.getChannel()))));
        }
        return super.packForBT(page);
    }

    /**
     * 获取信函数据管理管理列表
     */
    @RequestMapping(value = "/listAll")
    @ResponseBody
    public Object listAll() {
        List<Map<String, Object>> list = reportDataMapper.selectNoPageList(getRequestMapParams());
        for (Map<String, Object> r : list) {
            r.put("channelName", (ConstantFactory.me().getDictsByName(ConstantEnum.Enum_DictType.信函渠道.getCode(), Convert.toInt(r.get("channel")))));
        }
        return list;
    }


//    /**
//     * 获取信函数据管理管理列表
//     */
//    @GetMapping(value = "/export")
//    public Object export() {
//        List<Map<String, Object>> list = reportDataMapper.selectNoPageListForExcel(getRequestMapParams());
//        String fileName = DateUtil.getAllTime() + " 信函数据管理列表.xlsx";
//        String filePath = exportToExcel(fileName, list);
//        return renderFile(fileName, filePath);
//    }

    /**
     * 新增信函数据管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(ReportData reportData) {
        reportDataService.insert(reportData);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除信函数据管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Long reportDataId) {
        reportDataService.deleteById(reportDataId);
        return SUCCESS_TIP;
    }

    /**
     * 修改信函数据管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(ReportData reportData) {
        reportDataService.updateById(reportData);
        return super.SUCCESS_TIP;
    }

    /**
     * 信函数据管理详情
     */
    @RequestMapping(value = "/detail/{reportDataId}")
    @ResponseBody
    public Object detail(@PathVariable("reportDataId") Long reportDataId) {
        return reportDataService.selectById(reportDataId);
    }


    /**
     * 打印信函数据
     */
    @RequestMapping(value = "/print")
    @ResponseBody
    public Object print(@RequestParam Long reportDataId) {
        ReportData reportData = reportDataService.selectById(reportDataId);
        System.out.println(reportData.getFilePath());
        Tip tip = SUCCESS_TIP;
        try {
            String filePath = descFilePath + File.separator + reportData.getFilePath();
            File file = new File(filePath);
            if(file.exists()){
                String rs = CommandLineUtil.exeCmd("lp " + filePath);
                tip.setMessage(rs);
            }else{
                tip.setMessage("找不到指定的文件");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return tip;
    }

//    /**
//     * 模拟核心接口  打印功能
//     */
//    @RequestMapping(value = "/printList")
//    @ResponseBody
//    public Object printList() {
//        // 获取传输过来的模板类型
//        String reportType = getPara("reportType");
//        // 获取传输过来的分公司代码
//        String ogranCode = getPara("ogranCode");
//        // 根据分公司代码获取打印机构名称以及打印机编号
//        String name =  reportDataService.findByOrgan(ogranCode);
//        // 获取模板名字
//        String reportTemplateName = reportDataService.findNameByReportType(reportType);
//        // xml -->paf
//        transferHtml2PdfTask.transferHtml2Pdf(reportType);
//        //PrintUtils.printWithDialog(descFilePath+ File.separator +"新契约信函转账不成功通知函20180427162837.pdf");
//        return PREFIX + reportTemplateName;
//    }
    /**
     * 模拟核心接口  打印功能
     */
    @RequestMapping(value = "/printTest" ,produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public void printTest() {
        // 获取传输过来的模板类型
        String name = getPara("name");
        HtmlToPdfUtil.convert("http://localhost:8080/report/showTest?name="+name,"F:\\pdf\\"+name+".pdf");
    }
    /**
     * 模拟核心接口  打印功能
     */
    @RequestMapping(value = "/printTest1")
    @ResponseBody
    public Object printTest1(@RequestParam String name) {

        StringBuilder json = new StringBuilder();
        // 获取传输过来的模板类型
        File file = null;
        try {
            // 统一资源
            //URL url = new URL("http://10.0.21.76:8080/core/viewReport ");
            //URL url = new URL("http://localhost:8080/core/viewReport");
            URL url = new URL("http://10.0.30.39:8080/core/viewReport");
            // 连接类的父类，抽象类
            URLConnection urlConnection = url.openConnection();
            // http的连接类
            HttpURLConnection httpURLConnection = (HttpURLConnection) urlConnection;
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setInstanceFollowRedirects(true);
            httpURLConnection.setRequestProperty("Content-Type",
                    "application/json;charset=UTF-8");
            // 设置字符编码
            httpURLConnection.setRequestProperty("Charset", "UTF-8");
            // 打开到此 URL 引用的资源的通信链接（如果尚未建立这样的连接）。
            httpURLConnection.connect();
            DataOutputStream out1 = new DataOutputStream(
                    httpURLConnection.getOutputStream());
            JSONArray ja = new JSONArray();
            JSONObject obj = new JSONObject();
            obj.put("reportType", "1043_215");
            obj.put("organCode", "80");
            obj.put("channelType", "2");
            obj.put("reportData","<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "<ROOT>\n" +
                    "<LETTER_DETAILS>\n" +
                    "<LETTER_DETAIL>\n" +
                    "<CARD_NO><![CDATA[\"00000514792201031\"]]></CARD_NO>\n" +
                    "<QRCODE_DATA>TA:批单00000514792201031保单号00116004977008088客户金星一变更项解除合同生效日2019-03-01涉及金额15478.65元|2DnMjzHJaSOd3i6IfRJrZK9PYSNaARY60006</QRCODE_DATA><PH_NAME><![CDATA[\"金星一\"]]></PH_NAME>\n" +
                    "<PH_TITLE><![CDATA[\"女士\"]]></PH_TITLE>\n" +
                    "<PH_CERTI_CODE><![CDATA[\"00116004977008088\"]]></PH_CERTI_CODE>\n" +
                    "<INSURED_NAME><![CDATA[\"金星一\"]]></INSURED_NAME>\n" +
                    "<INSURED_CERTI_CODE><![CDATA[\"00116004977008088\"]]></INSURED_CERTI_CODE>\n" +
                    "<POLICY_NO><![CDATA[\"00116004977008088\"]]></POLICY_NO>\n" +
                    "<POLICY_TYPE><![CDATA[\"1\"]]></POLICY_TYPE>\n" +
                    "<APPLY_CODE><![CDATA[\"00000651919601029\"]]></APPLY_CODE>\n" +
                    "<APPLY_TIME><![CDATA[\"2019年03月01日\"]]></APPLY_TIME>\n" +
                    "<ENTER_OPERATOR><![CDATA[\"DAN\"]]></ENTER_OPERATOR>\n" +
                    "<APPROVER_NAME><![CDATA[\"XF.ZHANG\"]]></APPROVER_NAME>\n" +
                    "<SERVICE_ID><![CDATA[\"215\"]]></SERVICE_ID>\n" +
                    "<SERVICE_NAME><![CDATA[\"解除合同\"]]></SERVICE_NAME>\n" +
                    "<VALIDATE_TIME><![CDATA[\"2019年03月01日\"]]></VALIDATE_TIME>\n" +
                    "<BENEFITS_NAME>天安人寿吉祥树2号增强版两全保险、天安人寿附加吉祥树2号增强版终身重大疾病保险、天安人寿附加金如意C款年金保险（万能型）</BENEFITS_NAME>\n" +
                    "<ALTERATION_DESC><![CDATA[\"\"]]></ALTERATION_DESC>\n" +
                    "<PRODUCT_DETAILS><PRODUCT_DETAIL><PRODUCT_NAME>天安人寿吉祥树2号增强版两全保险</PRODUCT_NAME>\n" +
                    "<RETURN_PREM><![CDATA[\"1,905.24元\"]]></RETURN_PREM>\n" +
                    "<LOAN_AMOUNT><![CDATA[\"\"]]></LOAN_AMOUNT>\n" +
                    "<APL_AMOUNT><![CDATA[\"\"]]></APL_AMOUNT>\n" +
                    "<CB_AMOUNT><![CDATA[\"\"]]></CB_AMOUNT>\n" +
                    "<SB_AMOUNT><![CDATA[\"\"]]></SB_AMOUNT>\n" +
                    "<TERMINAL_INTEREST><![CDATA[\"\"]]></TERMINAL_INTEREST>\n" +
                    "</PRODUCT_DETAIL>\n" +
                    "<PRODUCT_DETAIL><PRODUCT_NAME>天安人寿附加吉祥树2号增强版终身重大疾病保险</PRODUCT_NAME>\n" +
                    "<RETURN_PREM><![CDATA[\"3,430.53元\"]]></RETURN_PREM>\n" +
                    "<LOAN_AMOUNT><![CDATA[\"\"]]></LOAN_AMOUNT>\n" +
                    "<APL_AMOUNT><![CDATA[\"\"]]></APL_AMOUNT>\n" +
                    "<CB_AMOUNT><![CDATA[\"\"]]></CB_AMOUNT>\n" +
                    "<SB_AMOUNT><![CDATA[\"\"]]></SB_AMOUNT>\n" +
                    "<TERMINAL_INTEREST><![CDATA[\"\"]]></TERMINAL_INTEREST>\n" +
                    "</PRODUCT_DETAIL>\n" +
                    "<PRODUCT_DETAIL><PRODUCT_NAME>天安人寿附加金如意C款年金保险（万能型）</PRODUCT_NAME>\n" +
                    "<RETURN_PREM><![CDATA[\"10,142.88元\"]]></RETURN_PREM>\n" +
                    "<LOAN_AMOUNT><![CDATA[\"\"]]></LOAN_AMOUNT>\n" +
                    "<APL_AMOUNT><![CDATA[\"\"]]></APL_AMOUNT>\n" +
                    "<CB_AMOUNT><![CDATA[\"\"]]></CB_AMOUNT>\n" +
                    "<SB_AMOUNT><![CDATA[\"\"]]></SB_AMOUNT>\n" +
                    "<TERMINAL_INTEREST><![CDATA[\"\"]]></TERMINAL_INTEREST>\n" +
                    "</PRODUCT_DETAIL>\n" +
                    "</PRODUCT_DETAILS>\n" +
                    "<HANDLING_FEE><![CDATA[\"0.00元\"]]></HANDLING_FEE>\n" +
                    "<TB_AMOUNT><![CDATA[\"\"]]></TB_AMOUNT>\n" +
                    "<POLICY_ACCOUNT><![CDATA[\"0.00元\"]]></POLICY_ACCOUNT>\n" +
                    "<PRODUCT_NAMES>天安人寿附加金如意C款年金保险（万能型）</PRODUCT_NAMES>\n" +
                    "<RETURN_PREM><![CDATA[\"15,478.65元\"]]></RETURN_PREM>\n" +
                    "<BANK_AND_SAVING_AGENCY></BANK_AND_SAVING_AGENCY>\n" +
                    "<BUSINESS_GROUP>宝山营业区风行部施云良组</BUSINESS_GROUP>\n" +
                    "<MARKETING_GROUP>上海分公司宝山支公司</MARKETING_GROUP>\n" +
                    "<AGENT_NAME>施云良</AGENT_NAME>\n" +
                    "<AGENT_CODE><![CDATA[\"100000001\"]]></AGENT_CODE>\n" +
                    "<PRINT_ORGAN>上海分公司</PRINT_ORGAN>\n" +
                    "<PRINT_DATE>2018年08月20日</PRINT_DATE>\n" +
                    "</LETTER_DETAIL>\n" +
                    "<LETTER_DETAIL></LETTER_DETAIL>\n" +
                    "</LETTER_DETAILS>\n" +
                    "</ROOT>");
            ja.add(obj);
          /*  ja.add(obj1);
            ja.add(obj2);
            ja.add(obj4);
            ja.add(obj5);*/
            JSONObject obj30 = new JSONObject();
            obj30.put("datas",ja);
            out1.write(obj30.toString().getBytes("UTF-8"));


            out1.flush();
            out1.close();

            // 文件大小
            int fileLength = httpURLConnection.getContentLength();

            // 文件名
            String filePathUrl = httpURLConnection.getURL().getFile();
            String fileFullName = filePathUrl.substring(filePathUrl.lastIndexOf(File.separatorChar) + 1);
            String context = httpURLConnection.getHeaderField("Content-Disposition");
            context = new String(context.getBytes("ISO-8859-1"),"UTF-8");
            String fileNameAndType = "";
            String fileName = "";
            String fileType = "";
            // 获取文件名
            if (context.indexOf("fileName")!=-1) {
                fileNameAndType = context.substring(context.indexOf("fileName"));
                if (fileNameAndType.startsWith("\"") && fileNameAndType.endsWith("\"")) {
                    fileNameAndType = fileNameAndType.substring(1,fileNameAndType.length()-1);
                }
            }
            fileName = fileNameAndType.substring(0,fileNameAndType.lastIndexOf("."));
            fileType = fileNameAndType.substring(fileNameAndType.lastIndexOf(".")+1);
            URLConnection con = url.openConnection();

            BufferedInputStream bin = new BufferedInputStream(httpURLConnection.getInputStream());

            String path = "F:/yizeng" + File.separatorChar + fileFullName;
            DataOutputStream os = new DataOutputStream (getHttpServletResponse().getOutputStream());
            getHttpServletResponse().setContentType("application/pdf");
            getHttpServletResponse().setHeader("Content-disposition", "attachment;"+fileName+"."+fileType);

            BufferedReader in = new BufferedReader(new InputStreamReader(bin,"UTF-8"));

            int size = 0;
            int len = 0;
            byte[] buf = new byte[1024];
            String inputLine = null;
            while ((size = bin.read(buf)) != -1) {
            //while ((inputLine = in.readLine()) != null) {
                //json.append(inputLine);

            len += size;
            os.write(buf, 0, size);
            }
            bin.close();
            os.close();
            //System.out.println(json);
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            return json;
        }
    }
}
