package com.stylefeng.guns.modular.report.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.persistence.model.ReportTemplate;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 信函模版 服务类
 * </p>
 *
 * @author zyy258963
 * @Date 2018-01-25 18:01:53
 */
public interface IReportTemplateService extends IService<ReportTemplate> {
    /**
     * 根据查询条件，查询当前表的列表
     * @param page
     * @param reportTemplate
     * @return
     */
    Page<ReportTemplate> selectPageList(Page<ReportTemplate> page, ReportTemplate reportTemplate);
}
