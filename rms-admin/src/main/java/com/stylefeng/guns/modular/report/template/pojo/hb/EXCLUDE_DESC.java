/**
  * Copyright 2018 bejson.com 
  */
package com.stylefeng.guns.modular.report.template.pojo.hb;

/**
 * Auto-generated: 2018-02-05 9:40:19
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class EXCLUDE_DESC {

    private String EXC_PRD_SEQ_NO;
    private String EXC_PRD_NAME;
    private String EXC_PRD_CODE;
    private String EXC_DESC;
    private String EXC_REASON;
    public void setEXC_PRD_SEQ_NO(String EXC_PRD_SEQ_NO) {
         this.EXC_PRD_SEQ_NO = EXC_PRD_SEQ_NO;
     }
     public String getEXC_PRD_SEQ_NO() {
         return EXC_PRD_SEQ_NO;
     }

    public void setEXC_PRD_NAME(String EXC_PRD_NAME) {
         this.EXC_PRD_NAME = EXC_PRD_NAME;
     }
     public String getEXC_PRD_NAME() {
         return EXC_PRD_NAME;
     }

    public void setEXC_PRD_CODE(String EXC_PRD_CODE) {
         this.EXC_PRD_CODE = EXC_PRD_CODE;
     }
     public String getEXC_PRD_CODE() {
         return EXC_PRD_CODE;
     }

    public void setEXC_DESC(String EXC_DESC) {
         this.EXC_DESC = EXC_DESC;
     }
     public String getEXC_DESC() {
         return EXC_DESC;
     }

    public void setEXC_REASON(String EXC_REASON) {
         this.EXC_REASON = EXC_REASON;
     }
     public String getEXC_REASON() {
         return EXC_REASON;
     }

}