/**
  * Copyright 2018 bejson.com 
  */
package com.stylefeng.guns.modular.report.template.pojo.hb;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Auto-generated: 2018-02-05 9:40:19
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@XmlRootElement(name="DECLINED")
public class DECLINED {

    private String DECL_PRODUCT_NAME;
    private String DECL_INTERAL_ID;
    private String DECL_REASON;
    private String DECL_SEQUEN;

    public DECLINED() {
    }

    public void setDECL_PRODUCT_NAME(String DECL_PRODUCT_NAME) {
         this.DECL_PRODUCT_NAME = DECL_PRODUCT_NAME;
     }
     public String getDECL_PRODUCT_NAME() {
         return DECL_PRODUCT_NAME;
     }

    public void setDECL_INTERAL_ID(String DECL_INTERAL_ID) {
         this.DECL_INTERAL_ID = DECL_INTERAL_ID;
     }
     public String getDECL_INTERAL_ID() {
         return DECL_INTERAL_ID;
     }

    public void setDECL_REASON(String DECL_REASON) {
         this.DECL_REASON = DECL_REASON;
     }
     public String getDECL_REASON() {
         return DECL_REASON;
     }

    public void setDECL_SEQUEN(String DECL_SEQUEN) {
         this.DECL_SEQUEN = DECL_SEQUEN;
     }
     public String getDECL_SEQUEN() {
         return DECL_SEQUEN;
     }

}