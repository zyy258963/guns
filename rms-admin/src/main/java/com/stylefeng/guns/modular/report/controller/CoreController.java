package com.stylefeng.guns.modular.report.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.util.Common;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.*;

@Controller
@RequestMapping("/core")
public class CoreController extends BaseController{

    @Value("${report.descFilePath}")
    private String descFilePath;
    @Autowired
    private Common common;
    /**
     * 信函预览接口
     */
    @RequestMapping(value = "/viewReport")
    public void previewReport(@RequestBody JSONObject jsonObject){

        JSONArray jsonEntitys = (JSONArray)jsonObject.getJSONArray("datas");
        // 解析json参数,转成pdf  flag:判断是那个接口调用
        common.xmlToPdf(jsonEntitys,getHttpServletResponse(),"1");
}
    /**
     * 信函打印接口
     */
    @RequestMapping(value = "/printReport")
    @ResponseBody
    public void printReport(@RequestBody JSONObject jsonObject){
        // 接收 json参数
        JSONArray jsonEntitys = (JSONArray)jsonObject.getJSONArray("datas");
        // 解析json参数,转成pdf flag:判断是那个接口调用
        common.xmlToPdf(jsonEntitys,getHttpServletResponse(),"2");
        // 向打印机输出文件
    }

    /**
     * 信函打印并预览接口
     */
    @RequestMapping(value = "/printAndViewReport")
    @ResponseBody
    public void printAndViewReport(@RequestBody JSONObject jsonObject){
        // 接收 json参数
        JSONArray jsonEntitys = (JSONArray)jsonObject.getJSONArray("datas");
        // 解析json参数,转成pdf  flag:判断是那个接口调用
        common.xmlToPdf(jsonEntitys,getHttpServletResponse(),"3");

    }

    /**
     * 测试是否宕机
     */
    @RequestMapping(value = "/monitorWithoutLogin")
    @ResponseBody
    public String monitorWithoutLogin(){

//        JSONObject jsonObject1 = new JSONObject();
//        jsonObject1.put("start","server is started! ");
//        jsonObject1.put("organ","天安人寿保险股份有限公司 ");
        String str = "server is started!  <br/>"+
                "天安人寿保险股份有限公司";
        return str;
    }
}
