package com.stylefeng.guns.modular.report.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.stylefeng.guns.common.persistence.model.ReportData;
import com.stylefeng.guns.common.persistence.model.ReportTemplate;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.shiro.ShiroKit;
import com.stylefeng.guns.core.util.JsonUtil;
import com.stylefeng.guns.modular.report.dao.ReportTemplateMapper;
import com.stylefeng.guns.modular.report.service.IReportDataService;
import com.stylefeng.guns.modular.report.service.IReportTemplateService;
import com.stylefeng.guns.modular.report.warpper.ReportTemplateWarpper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;

/**
 * 信函模版管理控制器
 *
 * @author fengshuonan
 * @Date 2018-01-24 17:55:29
 */
@Controller
@RequestMapping("/report")
public class ReportController extends BaseController {

    private String PREFIX = "/report/templates/";

    @Autowired
    private IReportDataService reportDataService;

    @Autowired
    private ReportTemplateMapper reportTemplateMapper;

    /**
     * 跳转到信函模版管理首页
     */
    @RequestMapping("/show")
    public String index() {
        String reportType = getPara("reportType");
        if (StringUtils.isNotEmpty(reportType)) {
            String dataId = getPara("dataId");
            ReportData reportData = reportDataService.selectById(dataId);
            JSONObject json = JSONObject.parseObject(reportData.getData());
            System.out.println(json.toString());
            for (String key : json.keySet()) {
                if ("CARD_NO".equals(key)) {
                    // 给cardNO 加空格
                    String regex = "(.{1})";
                    String input = (String)json.get(key);
                    input = input.replaceAll (regex, "$1 ");
                    setAttr(key, json.get(key));
                    setAttr("card_noStr", input);
                }else {
                    if ("CONTENTS".equals(key)) {
                        // CONTENTS自动换行
                        String input = (String)json.get(key);
                        input = input.replaceAll("\n","<br />");
                        setAttr(key, input);
                    } else if("POSTPHONE_INFO".equals(key) || "DECLINE_INFO".equals(key)){
                        // POSTPHONE_INFO自动换行
                        String input = (String)json.get(key);
                        input = input.replaceAll("\n","<br />");
                        setAttr(key, input);

                    } else if("HOSPITAL_LIST".equals(key)){
                        // POSTPHONE_INFO自动换行
                        if(JsonUtil.isJsonArray(json.get(key))){
                            JSONArray jc = new JSONArray();
                            JSONArray ja= (JSONArray)json.get(key);
                            for (int i =0;i<ja.size();i++) {
                                JSONObject job = ja.getJSONObject(i);
                                String ADDRESS=job.getString("ADDRESS");
                                if (ADDRESS !=null && !"".equals(ADDRESS)) {
                                    ADDRESS = ADDRESS.replaceAll("\n","<br />");
                                    job.put("ADDRESS",ADDRESS);
                                } else{
                                    job.put("ADDRESS",ADDRESS);
                                }
                                jc.add(job);
                            }
                            setAttr(key, ja);
                        } else {
                            JSONObject input = (JSONObject)json.get(key);
                            String ADDRESS = input.getString("ADDRESS");
                            if (ADDRESS !=null && !"".equals(ADDRESS)) {
                                ADDRESS = ADDRESS.replaceAll("\n","<br />");
                                input.put("ADDRESS",ADDRESS);
                                setAttr(key, input);
                            }
                            setAttr(key, json.get(key));
                        }
                    } else if ("LETTER_DETAILS".equals(key)){
                        JSONObject input = (JSONObject)json.get(key);
                        if (input!=null){
                            JSONObject input1 = (JSONObject)input.get("LETTER_DETAIL");
                            if(input1 != null) {
                                for (String key1 : input1.keySet()) {
                                    if ("COMMENTS".equals(key1)) {
                                        String comments = input1.getString(key1);
                                        if (comments != null) {
                                            comments = comments.replaceAll("\n", "<br />");
                                            input1.put("COMMENTS", comments);
                                        }
                                    }
                                }
                            }
                            input.put("LETTER_DETAIL",input1);
                            setAttr(key, input);
                        } else {
                            setAttr(key, json.get(key));
                        }
                    } else if ("ALTERATION_DESCS".equals(key)) {
                        JSONObject js = (JSONObject)json.get(key);
                        // 判断不为空，继续下面操作
                        if(js.get("ALTERATION_DESC")!=null || "".equals(js.get("ALTERATION_DESC"))){
                            if(JsonUtil.isJsonArray(js.get("ALTERATION_DESC"))) {
                                JSONArray jt= new JSONArray();
                                JSONArray ja = (JSONArray) js.get("ALTERATION_DESC");
                                for (int i =0;i<ja.size();i++) {
                                    JSONObject job = ja.getJSONObject(i);
                                    String txt=job.getString("TXT");
                                    if (txt !=null && !"".equals(txt)) {
                                        txt = txt.trim();
                                        txt = txt.replaceAll(" ","&ensp;");
                                        txt = txt.replaceAll("姓名:","姓名：");
                                        txt = txt.replaceAll("&ensp;性别：","性别：");
                                        job.put("TXT",txt);
                                    } else{
                                        job.put("TXT",txt);
                                    }
                                    jt.add(job);
                                }
                                js.put("ALTERATION_DESC",jt);
                                setAttr(key, js);
                            } else {
                                setAttr(key, json.get(key));
                            }
                        }else {
                            setAttr(key, json.get(key));
                        }
                    } else  if("COL_DESC".equals(key)){
                            String comments = json.getString(key);
                            if (comments != null) {
                                comments = comments.trim();
                                comments = comments.replaceAll(" ","&ensp;");
                                setAttr("COL_DESC", comments);
                            }
                    }
                    else {
                        setAttr(key, json.get(key));
                    }
                }

            }
            ReportTemplate reportParam = new ReportTemplate();
            if("1043".equals(reportType)){
                reportParam.setCode(reportType);
                reportParam.setServiceid(json.getString("SERVICE_ID"));
            } else {
                reportParam.setCode(reportType);
            }
            ReportTemplate reportTemplate = reportTemplateMapper.selectOne(reportParam);
            return PREFIX + reportTemplate.getPath();
        } else {
            return "500";
        }
    }

    /**
     * 跳转到信函模版管理首页
     */
    @RequestMapping("/showTest")
    public String indexTest() {
        String reportType = getPara("name");

        return PREFIX + reportType;
    }

}
