/**
  * Copyright 2018 bejson.com 
  */
package com.stylefeng.guns.modular.report.template.pojo.hb;

/**
 * Auto-generated: 2018-02-05 9:40:19
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class EXTRA_CHILD {

    private String RATE;
    private String EXTRA_PREM_TYPE;
    private String EXTRA_PREM_FREQ;
    private String EXTRA_PREM_AMOUNT;
    private String PRODUCT_SEQ_NO;
    public void setRATE(String RATE) {
         this.RATE = RATE;
     }
     public String getRATE() {
         return RATE;
     }

    public void setEXTRA_PREM_TYPE(String EXTRA_PREM_TYPE) {
         this.EXTRA_PREM_TYPE = EXTRA_PREM_TYPE;
     }
     public String getEXTRA_PREM_TYPE() {
         return EXTRA_PREM_TYPE;
     }

    public void setEXTRA_PREM_FREQ(String EXTRA_PREM_FREQ) {
         this.EXTRA_PREM_FREQ = EXTRA_PREM_FREQ;
     }
     public String getEXTRA_PREM_FREQ() {
         return EXTRA_PREM_FREQ;
     }

    public void setEXTRA_PREM_AMOUNT(String EXTRA_PREM_AMOUNT) {
         this.EXTRA_PREM_AMOUNT = EXTRA_PREM_AMOUNT;
     }
     public String getEXTRA_PREM_AMOUNT() {
         return EXTRA_PREM_AMOUNT;
     }

    public void setPRODUCT_SEQ_NO(String PRODUCT_SEQ_NO) {
         this.PRODUCT_SEQ_NO = PRODUCT_SEQ_NO;
     }
     public String getPRODUCT_SEQ_NO() {
         return PRODUCT_SEQ_NO;
     }

}