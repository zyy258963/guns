package com.stylefeng.guns.modular.report.controller;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.util.DateUtil;
import com.stylefeng.guns.modular.report.dao.OrganDataMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.common.persistence.model.OrganData;
import com.stylefeng.guns.modular.report.service.IOrganDataService;

import java.util.List;
import java.util.Map;

/**
 * 打印机信息管理控制器
 *
 * @author zyy258963
 * @Date 2018-07-25 15:20:25
 */
@Controller
@RequestMapping("/organData")
public class OrganDataController extends BaseController {

    private String PREFIX = "/report/organData/";

    @Autowired
    private IOrganDataService organDataService;

    @Autowired
    private OrganDataMapper organDataMapper;

    /**
     * 跳转到打印机信息管理首页
     */
    @RequestMapping("")
    public String index(Model model) {
        return PREFIX + "organData.html";
    }

    /**
     * 跳转到添加打印机信息管理
     */
    @RequestMapping("/organData_add")
    public String organDataAdd(Model model) {
        return PREFIX + "organData_add.html";
    }

    /**
     * 跳转到修改打印机信息管理
     */
    @RequestMapping("/organData_update/{organDataId}")
    public String organDataUpdate(@PathVariable Long organDataId, Model model) {
        OrganData organData = organDataService.selectById(organDataId);
        model.addAttribute("item",organData);
        LogObjectHolder.me().set(organData);
        return PREFIX + "organData_edit.html";
    }

    /**
     * 获取打印机信息管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(OrganData organData) {
        Page<OrganData> page = new PageFactory<OrganData>().defaultPage();
        page = organDataService.selectPageList(page,organData);
        return super.packForBT(page);
    }

    /**
     * 获取打印机信息管理管理列表
     */
    @RequestMapping(value = "/listAll")
    @ResponseBody
    public Object listAll() {
        List<Map<String, Object>> list = organDataMapper.selectNoPageList(getRequestMapParams());
        return list;
    }
//
//
//    /**
//     * 获取打印机信息管理管理列表
//     */
//    @GetMapping(value = "/export")
//    public Object export() {
//        List<Map<String, Object>> list = organDataMapper.selectNoPageListForExcel(getRequestMapParams());
//        String fileName = DateUtil.getAllTime() + " 打印机信息管理列表.xlsx";
//        String filePath = exportToExcel(fileName, list);
//        return renderFile(fileName, filePath);
//    }

    /**
     * 新增打印机信息管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(OrganData organData) {
        organDataService.insert(organData);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除打印机信息管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Long organDataId) {
        organDataService.deleteById(organDataId);
        return SUCCESS_TIP;
    }

    /**
     * 修改打印机信息管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(OrganData organData) {
        organDataService.updateById(organData);
        return super.SUCCESS_TIP;
    }

    /**
     * 打印机信息管理详情
     */
    @RequestMapping(value = "/detail/{organDataId}")
    @ResponseBody
    public Object detail(@PathVariable("organDataId") Long organDataId) {
        return organDataService.selectById(organDataId);
    }
}
