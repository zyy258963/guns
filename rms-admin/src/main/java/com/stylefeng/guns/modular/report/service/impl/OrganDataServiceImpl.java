package com.stylefeng.guns.modular.report.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.persistence.model.OrganData;
import com.stylefeng.guns.core.support.BeanKit;
import com.stylefeng.guns.modular.report.service.IOrganDataService;
import com.stylefeng.guns.modular.report.dao.OrganDataMapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zyy258963
 * @Date 2018-07-25 15:20:26
 */
@Service
public class OrganDataServiceImpl extends ServiceImpl<OrganDataMapper, OrganData> implements IOrganDataService {
    
    @Resource
    OrganDataMapper organDataMapper;

    /**
     * 根据查询条件，查询当前表的列表
     * @param page
     * @param organData
     * @return
     */
    public Page<OrganData> selectPageList(Page<OrganData> page, OrganData organData) {
        page.setRecords(organDataMapper.selectPageList(page, BeanKit.beanToMap(organData)));
        return page;
    }
}
