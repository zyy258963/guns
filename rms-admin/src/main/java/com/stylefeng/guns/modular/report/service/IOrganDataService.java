package com.stylefeng.guns.modular.report.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.persistence.model.OrganData;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类O
 * </p>
 *
 * @author zyy258963
 * @Date 2018-07-25 15:20:26
 */
public interface IOrganDataService extends IService<OrganData> {
    /**
     * 根据查询条件，查询当前表的列表
     * @param page
     * @param organData
     * @return
     */
    Page<OrganData> selectPageList(Page<OrganData> page, OrganData organData);
}
