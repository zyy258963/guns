/**
  * Copyright 2018 bejson.com 
  */
package com.stylefeng.guns.modular.report.template.pojo.hb;

/**
 * Auto-generated: 2018-02-05 9:40:19
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class LIMIT_PRD_LIST {

    private String LIMIT_PRD_SEQ_NO;
    private String LIMIT_PRD_NAME;
    private String LIMIT_PRD_CODE;
    private String LIMIT_PRD_TYPE;
    private String LIMIT_AMOUNT;
    private String LIMIT_PREM;
    private String LIMIT_REASON;
    public void setLIMIT_PRD_SEQ_NO(String LIMIT_PRD_SEQ_NO) {
         this.LIMIT_PRD_SEQ_NO = LIMIT_PRD_SEQ_NO;
     }
     public String getLIMIT_PRD_SEQ_NO() {
         return LIMIT_PRD_SEQ_NO;
     }

    public void setLIMIT_PRD_NAME(String LIMIT_PRD_NAME) {
         this.LIMIT_PRD_NAME = LIMIT_PRD_NAME;
     }
     public String getLIMIT_PRD_NAME() {
         return LIMIT_PRD_NAME;
     }

    public void setLIMIT_PRD_CODE(String LIMIT_PRD_CODE) {
         this.LIMIT_PRD_CODE = LIMIT_PRD_CODE;
     }
     public String getLIMIT_PRD_CODE() {
         return LIMIT_PRD_CODE;
     }

    public void setLIMIT_PRD_TYPE(String LIMIT_PRD_TYPE) {
         this.LIMIT_PRD_TYPE = LIMIT_PRD_TYPE;
     }
     public String getLIMIT_PRD_TYPE() {
         return LIMIT_PRD_TYPE;
     }

    public void setLIMIT_AMOUNT(String LIMIT_AMOUNT) {
         this.LIMIT_AMOUNT = LIMIT_AMOUNT;
     }
     public String getLIMIT_AMOUNT() {
         return LIMIT_AMOUNT;
     }

    public void setLIMIT_PREM(String LIMIT_PREM) {
         this.LIMIT_PREM = LIMIT_PREM;
     }
     public String getLIMIT_PREM() {
         return LIMIT_PREM;
     }

    public void setLIMIT_REASON(String LIMIT_REASON) {
         this.LIMIT_REASON = LIMIT_REASON;
     }
     public String getLIMIT_REASON() {
         return LIMIT_REASON;
     }

}