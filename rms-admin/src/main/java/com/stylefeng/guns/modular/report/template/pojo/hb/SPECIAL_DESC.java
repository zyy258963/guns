/**
  * Copyright 2018 bejson.com 
  */
package com.stylefeng.guns.modular.report.template.pojo.hb;

/**
 * Auto-generated: 2018-02-05 9:40:19
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class SPECIAL_DESC {

    private String SPECIAL_DESC_NO;
    private String CONDITION_DESC;
    private EXCLUDE_DESC EXCLUDE_DESC;
    public void setSPECIAL_DESC_NO(String SPECIAL_DESC_NO) {
         this.SPECIAL_DESC_NO = SPECIAL_DESC_NO;
     }
     public String getSPECIAL_DESC_NO() {
         return SPECIAL_DESC_NO;
     }

    public void setCONDITION_DESC(String CONDITION_DESC) {
         this.CONDITION_DESC = CONDITION_DESC;
     }
     public String getCONDITION_DESC() {
         return CONDITION_DESC;
     }

    public void setEXCLUDE_DESC(EXCLUDE_DESC EXCLUDE_DESC) {
         this.EXCLUDE_DESC = EXCLUDE_DESC;
     }
     public EXCLUDE_DESC getEXCLUDE_DESC() {
         return EXCLUDE_DESC;
     }

}