/**
 * 初始化信函数据管理详情对话框
 */
var ReportDataInfoDlg = {
    reportDataInfoData : {},
    validateFields: {}
};

/**
 * 清除数据
 */
ReportDataInfoDlg.clearData = function() {
    this.reportDataInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ReportDataInfoDlg.set = function(key, val) {
    this.reportDataInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ReportDataInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
ReportDataInfoDlg.close = function() {
    parent.layer.close(window.parent.ReportData.layerIndex);
}

/**
 * 收集数据
 */
ReportDataInfoDlg.collectData = function() {

    this.set('id');
    this.set('channel');
    this.set('code');
    this.set('sendTime');
    this.set('data');
    this.set('filePath');
    this.set('organ');
    this.set('createTime');
}

/**
 * 验证数据是否正确
 */
ReportDataInfoDlg.validate = function () {
    $('#reportDataInfoForm').data("bootstrapValidator").resetForm();
    $('#reportDataInfoForm').bootstrapValidator('validate');
    return $("#reportDataInfoForm").data('bootstrapValidator').isValid();
};

/**
 * 提交添加
 */
ReportDataInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    if (!this.validate()) {
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/reportData/add", function(data){
        Feng.success("添加成功!");
        window.parent.ReportData.table.refresh();
        ReportDataInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.reportDataInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
ReportDataInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    if (!this.validate()) {
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/reportData/update", function(data){
        Feng.success("修改成功!");
        window.parent.ReportData.table.refresh();
        ReportDataInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.reportDataInfoData);
    ajax.start();
}

$(function() {
    Feng.initValidator("ReportDataInfoForm", ReportDataInfoDlg.validateFields);
});
