/**
 * 信函数据管理管理初始化
 */
var ReportData = {
    id: "ReportDataTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
ReportData.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: '主键', field: 'id', visible: false, align: 'center', valign: 'middle'},
        {title: '对接渠道', field: 'channel', visible: false, align: 'center', valign: 'middle'},
        {title: '机构编码', field: 'organ', visible: true, align: 'center', valign: 'middle'},
        {title: '对接渠道', field: 'channelName', visible: true, align: 'center', valign: 'middle'},
        {title: '信函类型', field: 'code', visible: false, align: 'center', valign: 'middle'},
        {title: '信函类型', field: 'codeName', visible: true, align: 'center', valign: 'middle'},
        {title: '发送时间', field: 'sendTime', visible: true, align: 'center', valign: 'middle'},
        {title: '传输的数据', field: 'data', visible: true, align: 'center', valign: 'middle'},
        {
            title: '生成文件路径',
            field: 'filePath',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: function (value, row, index) {
                return '<a href="javascript:;" onclick="downloadFile(\'' + value + '\')" >下载</a>';
            }
        },
        {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 下载 报告文件
 */
function downloadFile(filePath) {
    window.open("/download/reportData?filePath=" + filePath);
}

/**
 * 检查是否选中
 */
ReportData.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length == 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        ReportData.seItem = selected[0];
        return true;
    }
};

/**
 * 打印信函数据
 */
ReportData.printDataFile = function () {
    if (this.check()) {
        //提交信息
        var ajax = new $ax(Feng.ctxPath + "/reportData/print", function(data){
            Feng.success("打印成功!");
            ExpenseInfoDlg.close();
        },function(data){
            Feng.error("打印失败!" + data.responseJSON.message + "!");
        });
        ajax.set("reportDataId", ReportData.seItem.id);
        ajax.start();
    }
}


/**
 * 点击添加信函数据管理
 */
ReportData.openAddReportData = function () {
    var index = layer.open({
        type: 2,
        title: '添加信函数据管理',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/reportData/reportData_add'
    });
    this.layerIndex = index;
};



/**
 * 打开查看信函数据管理详情
 */
ReportData.openReportDataDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '信函数据管理详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/reportData/reportData_update/' + ReportData.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除信函数据管理
 */
ReportData.delete = function () {
    if (this.check()) {
        Feng.confirm("是否删除 ?", function () {
            var ajax = new $ax(Feng.ctxPath + "/reportData/delete", function (data) {
                Feng.success("删除成功!");
                ReportData.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("reportDataId", ReportData.seItem.id);
            ajax.start();
        })
    }
};

/**
 * 查询信函数据管理列表
 */
ReportData.search = function () {
    var queryData = {};
    queryData['channel'] = $("#channel").val();
    queryData['code'] = $("#code").val();
    queryData['sendTime'] = $("#sendTime").val();
    queryData['data'] = $("#data").val();
    queryData['filePath'] = $("#filePath").val();
    queryData['createTime'] = $("#createTime").val();
    ReportData.table.refresh({query: queryData});
};

/**
 * 清空信函数据管理 查询条件
 */
ReportData.clear = function () {
    $("#channel").val("");
    $("#code").val("");
    $("#sendTime").val("");
    $("#data").val("");
    $("#filePath").val("");
    $("#createTime").val("");
};

/**
 * 导出excel
 */
ReportData.export = function () {
    var downUrl = Feng.ctxPath + "/reportData/export?1=1";
    downUrl += "&channel=" + $("#channel").val();
    downUrl += "&code=" + $("#code").val();
    downUrl += "&sendTime=" + $("#sendTime").val();
    downUrl += "&data=" + $("#data").val();
    downUrl += "&filePath=" + $("#filePath").val();
    downUrl += "&createTime=" + $("#createTime").val();
    window.location.href = downUrl;
}

$(function () {
    var defaultColunms = ReportData.initColumn();
    var table = new BSTable(ReportData.id, "/reportData/list", defaultColunms);
    table.setPaginationType("server");
    ReportData.table = table.init();
});
