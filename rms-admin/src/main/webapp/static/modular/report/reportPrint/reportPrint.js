/**
 * 信函数据管理管理初始化
 */
var ReportPrint = {
};


/**
 * 打印信函数据
 */
ReportPrint.execCmd = function () {
     var inputStr = $('#inputStr').val();
     $('#outputStr').val("");
     console.log(inputStr + " ================ ");
     if(!inputStr || inputStr==''){
         Feng.error("请输入执行语句!");
         return;
     }else{
         //提交信息
         var ajax = new $ax(Feng.ctxPath + "/reportPrint/test", function(data){
             Feng.success("执行成功!");
             $("#outputStr").val(data.message || "");
             ExpenseInfoDlg.close();
         },function(data){
             Feng.error("打印失败!" + data.responseJSON.message + "!");
         });
         ajax.set("cmdStr", inputStr);
         ajax.start();
     }

}
