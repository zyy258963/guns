/**
 * 信函模版管理管理初始化
 */
var ReportTemplate = {
    id: "ReportTemplateTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
ReportTemplate.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
                {title: '主键', field: 'id', visible: false, align: 'center', valign: 'middle'},
                {title: '模版名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
                {title: '模版编码', field: 'code', visible: true, align: 'center', valign: 'middle'},
                {title: '模版路径', field: 'path', visible: true, align: 'center', valign: 'middle'},
                {title: '模版版本', field: 'version', visible: true, align: 'center', valign: 'middle'},
        {title: '业务模块', field: 'moduleName', visible: true, align: 'center', valign: 'middle'},
        {title: '业务模块', field: 'module', visible: false, align: 'center', valign: 'middle'},
                {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
        {title: '创建人', field: 'operator', visible: false, align: 'center', valign: 'middle'},
        {title: '创建人', field: 'operatorName', visible: true, align: 'center', valign: 'middle'},
        {title: '状态', field: 'statusName', visible: true, align: 'center', valign: 'middle'},
        {title: '状态', field: 'status', visible: false, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
ReportTemplate.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        ReportTemplate.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加信函模版管理
 */
ReportTemplate.openAddReportTemplate = function () {
    var index = layer.open({
        type: 2,
        title: '添加信函模版管理',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/reportTemplate/reportTemplate_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看信函模版管理详情
 */
ReportTemplate.openReportTemplateDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '信函模版管理详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/reportTemplate/reportTemplate_update/' + ReportTemplate.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除信函模版管理
 */
ReportTemplate.delete = function () {
    if (this.check()) {
        Feng.confirm("是否删除 ?",function(){
            var ajax = new $ax(Feng.ctxPath + "/reportTemplate/delete", function (data) {
                Feng.success("删除成功!");
                ReportTemplate.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("reportTemplateId",ReportTemplate.seItem.id);
            ajax.start();
        })
    }
};

/**
 * 查询信函模版管理列表
 */
ReportTemplate.search = function () {
    var queryData = {};
         queryData['name'] = $("#name").val();
         queryData['code'] = $("#code").val();
         queryData['path'] = $("#path").val();
         queryData['version'] = $("#version").val();
         queryData['module'] = $("#module").val();
         queryData['createTime'] = $("#createTime").val();
         queryData['operator'] = $("#operator").val();
         queryData['status'] = $("#status").val();
    ReportTemplate.table.refresh({query: queryData});
};

/**
 * 清空信函模版管理 查询条件
 */
ReportTemplate.clear = function () {
         $("#name").val("");
         $("#code").val("");
         $("#path").val("");
         $("#version").val("");
         $("#module").val("");
         $("#createTime").val("");
         $("#operator").val("");
         $("#status").val("");
};

/**
 * 导出excel
 */
ReportTemplate.export = function (){
    var downUrl = Feng.ctxPath + "/reportTemplate/export?1=1";
         downUrl += "&name="+ $("#name").val();
         downUrl += "&code="+ $("#code").val();
         downUrl += "&path="+ $("#path").val();
         downUrl += "&version="+ $("#version").val();
         downUrl += "&module="+ $("#module").val();
         downUrl += "&createTime="+ $("#createTime").val();
         downUrl += "&operator="+ $("#operator").val();
         downUrl += "&status="+ $("#status").val();
    window.location.href = downUrl;
}

$(function () {
    var defaultColunms = ReportTemplate.initColumn();
    var table = new BSTable(ReportTemplate.id, "/reportTemplate/listAll", defaultColunms);
    table.setPaginationType("client");
    ReportTemplate.table = table.init();
});
