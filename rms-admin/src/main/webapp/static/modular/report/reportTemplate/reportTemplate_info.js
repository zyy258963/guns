/**
 * 初始化信函模版管理详情对话框
 */
var ReportTemplateInfoDlg = {
    reportTemplateInfoData : {},
    validateFields: {
        name: {
            validators: {
                notEmpty: {
                    message: '模版名称不能为空'
                }
            }
        }
    }
};

/**
 * 清除数据
 */
ReportTemplateInfoDlg.clearData = function() {
    this.reportTemplateInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ReportTemplateInfoDlg.set = function(key, val) {
    this.reportTemplateInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ReportTemplateInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
ReportTemplateInfoDlg.close = function() {
    parent.layer.close(window.parent.ReportTemplate.layerIndex);
}

/**
 * 收集数据
 */
ReportTemplateInfoDlg.collectData = function() {

    this.set('id');
    this.set('name');
    this.set('code');
    this.set('path');
    this.set('version');
    this.set('module');
    this.set('createTime');
    this.set('operator');
    this.set('status');
}

/**
 * 验证数据是否正确
 */
ReportTemplateInfoDlg.validate = function () {
    $('#reportTemplateInfoForm').data("bootstrapValidator").resetForm();
    $('#reportTemplateInfoForm').bootstrapValidator('validate');
    return $("#reportTemplateInfoForm").data('bootstrapValidator').isValid();
};

/**
 * 提交添加
 */
ReportTemplateInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    if (!this.validate()) {
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/reportTemplate/add", function(data){
        Feng.success("添加成功!");
        window.parent.ReportTemplate.table.refresh();
        ReportTemplateInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.reportTemplateInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
ReportTemplateInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    if (!this.validate()) {
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/reportTemplate/update", function(data){
        Feng.success("修改成功!");
        window.parent.ReportTemplate.table.refresh();
        ReportTemplateInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.reportTemplateInfoData);
    ajax.start();
}

$(function() {
    Feng.initValidator("reportTemplateInfoForm", ReportTemplateInfoDlg.validateFields);
});
