/**
 * 初始化打印机信息管理详情对话框
 */
var OrganDataInfoDlg = {
    organDataInfoData : {},
    validateFields: {}
};

/**
 * 清除数据
 */
OrganDataInfoDlg.clearData = function() {
    this.organDataInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
OrganDataInfoDlg.set = function(key, val) {
    this.organDataInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
OrganDataInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
OrganDataInfoDlg.close = function() {
    parent.layer.close(window.parent.OrganData.layerIndex);
}

/**
 * 收集数据
 */
OrganDataInfoDlg.collectData = function() {

    this.set('id');
    this.set('organCode');
    this.set('organName');
    this.set('printModel');
    this.set('ip');
    this.set('printCode');
    this.set('description');
}

/**
 * 验证数据是否正确
 */
OrganDataInfoDlg.validate = function () {
    //$('#organDataInfoForm').data("bootstrapValidator").resetForm();
    $('#organDataInfoForm').bootstrapValidator('validate');
    return $("#organDataInfoForm").data('bootstrapValidator').isValid();
};

/**
 * 提交添加
 */
OrganDataInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    if (!this.validate()) {
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/organData/add", function(data){
        Feng.success("添加成功!");
        window.parent.OrganData.table.refresh();
        OrganDataInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.organDataInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
OrganDataInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    if (!this.validate()) {
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/organData/update", function(data){
        Feng.success("修改成功!");
        window.parent.OrganData.table.refresh();
        OrganDataInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.organDataInfoData);
    ajax.start();
}

$(function() {
    Feng.initValidator("OrganDataInfoForm", OrganDataInfoDlg.validateFields);
});
