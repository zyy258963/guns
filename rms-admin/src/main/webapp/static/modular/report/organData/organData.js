/**
 * 打印机信息管理管理初始化
 */
var OrganData = {
    id: "OrganDataTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
OrganData.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
                {title: '主键', field: 'id', visible: false, align: 'center', valign: 'middle'},
                {title: '打印机构编号', field: 'organ_code', visible: true, align: 'center', valign: 'middle'},
                {title: '打印机构名称', field: 'organ_name', visible: true, align: 'center', valign: 'middle'},
                {title: '打印机型号', field: 'print_model', visible: true, align: 'center', valign: 'middle'},
                {title: 'IP地址', field: 'ip', visible: true, align: 'center', valign: 'middle'},
                {title: '打印机号码，打印name', field: 'print_code', visible: true, align: 'center', valign: 'middle'},
            {title: '描述', field: 'description', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
OrganData.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        OrganData.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加打印机信息管理
 */
OrganData.openAddOrganData = function () {
    var index = layer.open({
        type: 2,
        title: '添加打印机信息管理',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/organData/organData_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看打印机信息管理详情
 */
OrganData.openOrganDataDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '打印机信息管理详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/organData/organData_update/' + OrganData.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除打印机信息管理
 */
OrganData.delete = function () {
    if (this.check()) {
        Feng.confirm("是否删除 ?",function(){
            var ajax = new $ax(Feng.ctxPath + "/organData/delete", function (data) {
                Feng.success("删除成功!");
                OrganData.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("organDataId",OrganData.seItem.id);
            ajax.start();
        })
    }
};

/**
 * 查询打印机信息管理列表
 */
OrganData.search = function () {
    var queryData = {};
         queryData['organ_code'] = $("#organCode").val();
         queryData['organ_name'] = $("#organName").val();
         queryData['print_model'] = $("#printModel").val();
         queryData['ip'] = $("#ip").val();
         queryData['print_code'] = $("#printCode").val();
         queryData['description'] = $("#description").val();
    OrganData.table.refresh({query: queryData});
};

/**
 * 清空打印机信息管理 查询条件
 */
OrganData.clear = function () {
         $("#organCode").val("");
         $("#organName").val("");
         $("#printModel").val("");
         $("#ip").val("");
         $("#printCode").val("");
         $("#description").val("");
};

/**
 * 导出excel
 */
OrganData.export = function (){
    var downUrl = Feng.ctxPath + "/organData/export?1=1";
         downUrl += "&organCode="+ $("#organCode").val();
         downUrl += "&organName="+ $("#organName").val();
         downUrl += "&printModel="+ $("#printModel").val();
         downUrl += "&ip="+ $("#ip").val();
         downUrl += "&printCode="+ $("#printCode").val();
         downUrl += "&description="+ $("#description").val();
    window.location.href = downUrl;
}

$(function () {
    var defaultColunms = OrganData.initColumn();
    var table = new BSTable(OrganData.id, "/organData/listAll", defaultColunms);
    table.setPaginationType("client");
    OrganData.table = table.init();
});
