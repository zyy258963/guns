package com.stylefeng.guns.xml2json;

import com.alibaba.fastjson.JSONObject;
import com.stylefeng.guns.modular.report.template.pojo.hb.DECLINED;
import com.stylefeng.guns.modular.report.template.pojo.hb.DECLINED_POSTPINED;
import com.stylefeng.guns.modular.report.template.pojo.hb.LIMIT_DESC;
import com.stylefeng.guns.modular.report.template.pojo.hb.POSTPONED;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileInputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class Xml2JavaBean {

    public static void main(String[] args) throws Exception {
        String xmlStr = readFile("C:\\Users\\Administrator\\Desktop\\1.xml");
        System.out.println(xmlStr);
        Xml2JavaBean xml2JavaBean = new Xml2JavaBean();
        DECLINED_POSTPINED temp = (DECLINED_POSTPINED) xml2JavaBean.xml2Java(DECLINED_POSTPINED.class, xmlStr);
        System.out.println(JSONObject.toJSONString(temp));
    }


    public static String readFile(String path) throws Exception {
        File file = new File(path);
        FileInputStream fis = new FileInputStream(file);
        FileChannel fc = fis.getChannel();
        ByteBuffer bb = ByteBuffer.allocate(new Long(file.length()).intValue());
        //fc向buffer中读入数据
        fc.read(bb);
        bb.flip();
        String str = new String(bb.array(), "UTF8");
        fc.close();
        fis.close();
        return str;

    }

    /**
     * Java类向XML进行转换
     *
     * @param cls
     * @param obj
     * @throws JAXBException
     */
    public static <T> String java2Xml(Class<T> cls, Object obj) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(cls);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        StringWriter writer = new StringWriter();
        marshaller.marshal(obj, writer);
        return writer.toString();
    }

    /**
     * XML向JAVA类转换
     *
     * @param cls
     * @param content
     * @return
     * @throws JAXBException
     */
    public static <T> Object xml2Java(Class<T> cls, String content) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(cls);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
//        unmarshaller.setProperty();
        return unmarshaller.unmarshal(new StringReader(content));
    }
}
