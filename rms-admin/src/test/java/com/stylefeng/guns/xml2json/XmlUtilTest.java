package com.stylefeng.guns.xml2json;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdom2.*;
import org.jdom2.input.SAXBuilder;
import org.xml.sax.InputSource;

public class XmlUtilTest {

    public static void main(String[] args) {
        String str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<ROOT>\n" +
                "    <LETTER_DETAILS>\n" +
                "        <LETTER_DETAIL>\n" +
                "            <CARD_NO>\n" +
                "                <![CDATA[\"00000142738201019\"]]>\n" +
                "            </CARD_NO>\n" +
                "            <PH_NAME>\n" +
                "                <![CDATA[\"邵明静\"]]>\n" +
                "            </PH_NAME>\n" +
                "            <PH_TITLE>女士</PH_TITLE>\n" +
                "            <CERTI_CODE>\n" +
                "                <![CDATA[\"371122198005142847\"]]>\n" +
                "            </CERTI_CODE>\n" +
                "            <POLICY_CODE>\n" +
                "                <![CDATA[\"00000991423603036\"]]>\n" +
                "            </POLICY_CODE>\n" +
                "            <CHANNEL_TYPE>1</CHANNEL_TYPE>\n" +
                "            <INSURED_NAME>\n" +
                "                <![CDATA[\"邵明静\"]]>\n" +
                "            </INSURED_NAME>\n" +
                "            <INSURED_CERTI_CODE>\n" +
                "                <![CDATA[\"371122198005142847\"]]>\n" +
                "            </INSURED_CERTI_CODE>\n" +
                "            <EXTRA_PREM_DESC>\n" +
                "                <EXTRA_PREM_DESC_NO>\n" +
                "                    <![CDATA[\"999\"]]>\n" +
                "                </EXTRA_PREM_DESC_NO>\n" +
                "                <PRODUCT_INFO>\n" +
                "                    <EXTRA_CHILD>\n" +
                "                        <RATE>0</RATE>\n" +
                "                        <EXTRA_PREM_TYPE>empty</EXTRA_PREM_TYPE>\n" +
                "                        <EXTRA_PREM_FREQ>趸交</EXTRA_PREM_FREQ>\n" +
                "                        <EXTRA_PREM_AMOUNT>9.99</EXTRA_PREM_AMOUNT>\n" +
                "                        <PRODUCT_SEQ_NO>\n" +
                "                            <![CDATA[\"999\"]]>\n" +
                "                        </PRODUCT_SEQ_NO>\n" +
                "                    </EXTRA_CHILD>\n" +
                "                    <EXTRA_CHILD></EXTRA_CHILD>\n" +
                "                    <PRODUCT_SEQ_NO>\n" +
                "                        <![CDATA[\"999\"]]>\n" +
                "                    </PRODUCT_SEQ_NO>\n" +
                "                    <EXTRA_PREM_START_DATE>empty</EXTRA_PREM_START_DATE>\n" +
                "                    <EXTRA_PREM_END_DATE>empty</EXTRA_PREM_END_DATE>\n" +
                "                    <PREM_REASON>empty</PREM_REASON>\n" +
                "                    <ISU>0</ISU>\n" +
                "                    <PRODUCT_NAME>empty</PRODUCT_NAME>\n" +
                "                    <INTERNAL_CODE>empty</INTERNAL_CODE>\n" +
                "                </PRODUCT_INFO>\n" +
                "                <PRODUCT_INFO></PRODUCT_INFO>\n" +
                "                <EXTRA_PREM_TOTAL_AMOUNT>0.00</EXTRA_PREM_TOTAL_AMOUNT>\n" +
                "                <PREM_TOTAL_AMOUNT>4694.02</PREM_TOTAL_AMOUNT>\n" +
                "            </EXTRA_PREM_DESC>\n" +
                "            <EXTRA_PREM_DESC></EXTRA_PREM_DESC>\n" +
                "            <SPECIAL_DESC>\n" +
                "                <SPECIAL_DESC_NO>\n" +
                "                    <![CDATA[\"999\"]]>\n" +
                "                </SPECIAL_DESC_NO>\n" +
                "                <CONDITION_DESC>empty</CONDITION_DESC>\n" +
                "                <EXCLUDE_DESC>\n" +
                "                    <EXC_PRD_SEQ_NO>\n" +
                "                        <![CDATA[\"999\"]]>\n" +
                "                    </EXC_PRD_SEQ_NO>\n" +
                "                    <EXC_PRD_NAME>empty</EXC_PRD_NAME>\n" +
                "                    <EXC_PRD_CODE>empty</EXC_PRD_CODE>\n" +
                "                    <EXC_DESC>empty</EXC_DESC>\n" +
                "                    <EXC_REASON>empty</EXC_REASON>\n" +
                "                </EXCLUDE_DESC>\n" +
                "                <EXCLUDE_DESC></EXCLUDE_DESC>\n" +
                "            </SPECIAL_DESC>\n" +
                "            <SPECIAL_DESC></SPECIAL_DESC>\n" +
                "            <LIMIT_DESC>\n" +
                "                <LIMIT_PRD_LIST>\n" +
                "                    <LIMIT_PRD_SEQ_NO>\n" +
                "                        <![CDATA[\"999\"]]>\n" +
                "                    </LIMIT_PRD_SEQ_NO>\n" +
                "                    <LIMIT_PRD_NAME>empty</LIMIT_PRD_NAME>\n" +
                "                    <LIMIT_PRD_CODE>empty</LIMIT_PRD_CODE>\n" +
                "                    <LIMIT_PRD_TYPE>empty</LIMIT_PRD_TYPE>\n" +
                "                    <LIMIT_AMOUNT>empty</LIMIT_AMOUNT>\n" +
                "                    <LIMIT_PREM>9.99</LIMIT_PREM>\n" +
                "                    <LIMIT_REASON>empty</LIMIT_REASON>\n" +
                "                </LIMIT_PRD_LIST>\n" +
                "                <LIMIT_PRD_LIST></LIMIT_PRD_LIST>\n" +
                "                <LIMIT_PRD_SEQ_NO>\n" +
                "                    <![CDATA[\"999\"]]>\n" +
                "                </LIMIT_PRD_SEQ_NO>\n" +
                "            </LIMIT_DESC>\n" +
                "            <LIMIT_DESC></LIMIT_DESC>\n" +
                "            <DECLINED_POSTPINED>\n" +
                "                <POSTPONED>\n" +
                "                    <POST_PRODUCT_NAME>天安人寿附加住院费用医疗保险</POST_PRODUCT_NAME>\n" +
                "                    <POST_INTERNAL_ID>10131002</POST_INTERNAL_ID>\n" +
                "                    <POST_SEQUEN>\n" +
                "                        <![CDATA[\"1\"]]>\n" +
                "                    </POST_SEQUEN>\n" +
                "                    <POST_REASON>原因为：甲功异常</POST_REASON>\n" +
                "                </POSTPONED>\n" +
                "                <DECLINED>\n" +
                "                    <DECL_PRODUCT_NAME>empty</DECL_PRODUCT_NAME>\n" +
                "                    <DECL_INTERAL_ID>empty</DECL_INTERAL_ID>\n" +
                "                    <DECL_REASON>empty</DECL_REASON>\n" +
                "                    <DECL_SEQUEN>\n" +
                "                        <![CDATA[\"999\"]]>\n" +
                "                    </DECL_SEQUEN>\n" +
                "                </DECLINED>\n" +
                "                <DECLINED></DECLINED>\n" +
                "                <POSTPONED></POSTPONED>\n" +
                "                <SEQ_NO>\n" +
                "                    <![CDATA[\"1\"]]>\n" +
                "                </SEQ_NO>\n" +
                "            </DECLINED_POSTPINED>\n" +
                "            <DECLINED_POSTPINED></DECLINED_POSTPINED>\n" +
                "            <BANK_AND_SAVING_AGENCY></BANK_AND_SAVING_AGENCY>\n" +
                "            <BUSINESS_GROUP>日照龙腾一区焦秀娟部邵明连组</BUSINESS_GROUP>\n" +
                "            <MARKETING_GROUP>山东分公司日照中心支公司营业部</MARKETING_GROUP>\n" +
                "            <AGENT_NAME>邵明静</AGENT_NAME>\n" +
                "            <AGENT_CODE>\n" +
                "                <![CDATA[\"100296836\"]]>\n" +
                "            </AGENT_CODE>\n" +
                "            <PRINT_ORGAN>山东分公司日照中心支公司</PRINT_ORGAN>\n" +
                "            <PRINT_DATE>2018年01月30日</PRINT_DATE>\n" +
                "        </LETTER_DETAIL>\n" +
                "        <LETTER_DETAIL></LETTER_DETAIL>\n" +
                "    </LETTER_DETAILS>\n" +
                "</ROOT>\n";

        SAXBuilder builder = new SAXBuilder();
        try {
            Document document = builder.build(new StringReader(str));
            Element root = document.getRootElement();
            Element data = root.getChild("LETTER_DETAILS");

            //
            // Reading the mixed content of an xml element and iterate
            // the result list. This list object can contains any of the
            // following objects: Comment, Element, CDATA, DocType,
            // ProcessingInstruction, EntityRef and Text.
            //
            List content = data.getContent();
            String result = "";
            for (Object o : content) {
                if (o instanceof Comment) {
                    Comment comment = (Comment) o;
                    System.out.println("Comment   = " + comment);
                } else if (o instanceof Element) {
                    Element element = (Element) o;
                    System.out.println("Element   = " + element);
                } else if (o instanceof CDATA) {
                    CDATA cdata = (CDATA) o;
                    result = cdata.getText();
                    System.out.println("CDATA     = " + result);
                } else if (o instanceof DocType) {
                    DocType docType = (DocType) o;
                    System.out.println("DocType   = " + docType);
                } else if (o instanceof ProcessingInstruction) {
                    ProcessingInstruction pi = (ProcessingInstruction) o;
                    System.out.println("PI        = " + pi);
                } else if (o instanceof EntityRef) {
                    EntityRef entityRef = (EntityRef) o;
                    System.out.println("EntityRef = " + entityRef);
                } else if (o instanceof Text) {
                    Text text = (Text) o;
                    System.out.println("Text      = " + text);
                }
            }
        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
