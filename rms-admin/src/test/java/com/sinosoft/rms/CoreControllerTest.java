package com.sinosoft.rms;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.stylefeng.guns.core.util.HttpDownload;


public class CoreControllerTest {

    public static void main(String[] args) {
        JSONObject paramData = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONObject json = new JSONObject();
        json.put("reportType", "1065");
        json.put("organCode", "8006");
        json.put("channelType", "2");
        json.put("reportData", "<?xml version=\"1.0\" encoding=\"UTF-8\"?><ROOT><LETTER_DETAILS><LETTER_DETAIL" +
                "><CARD_NO><![CDATA[\"00083157893801031\"]]></CARD_NO><QRCODE_DATA>TA:批单00083157893801031" +
                "保单号00146909365808088客户王静丽变更项生存金、红利领取方式变更及给付授权生效日2018-08-21|7K65GbsXD6CBaGuYudxVcK9PYSNaARY60006" +
                "</QRCODE_DATA><PH_NAME><![CDATA[\"王静丽\"]]></PH_NAME><INSURED_NAME><![CDATA[\"王静丽\"]]></INSURED_NAME" +
                "><POLICY_NO><![CDATA[\"00146909365808088\"]]></POLICY_NO><POLICY_TYPE><![CDATA[\"3\"]]></POLICY_TYPE" +
                "><APPLY_CODE><![CDATA[\"00088500321201029\"]]></APPLY_CODE><APPLY_TIME><![CDATA[\"2018年08月21" +
                "日\"]]></APPLY_TIME><ENTER_OPERATOR><![CDATA[\"李天天\"]]></ENTER_OPERATOR><APPROVER_NAME><![CDATA" +
                "[\"李天天\"]]></APPROVER_NAME><SERVICE_ID><![CDATA[\"105\"]]></SERVICE_ID><SERVICE_NAME><![CDATA" +
                "[\"生存金、红利领取方式变更及给付授权\"]]></SERVICE_NAME><VALIDATE_TIME><![CDATA[\"2018年08月21日\"]]></VALIDATE_TIME" +
                "><ALTERATION_DESCS><ALTERATION_DESC><TXT>变更项目：生存金、红利领取方式变更及给付授权</TXT></ALTERATION_DESC" +
                "><ALTERATION_DESC><TXT>本次生存金、红利领取方式变更及给付授权内容经公司评估：</TXT></ALTERATION_DESC><ALTERATION_DESC><TXT" +
                ">同意在我公司进行投保的天安人寿逸享人生年金保险进行年金领取选项变更。变更年金领取方式为以现金收生存险种。</TXT></ALTERATION_DESC><ALTERATION_DESC><TXT" +
                ">同意在我公司进行投保的天安人寿逸享人生年金保险进行年金给付授权。授权年金领取方式为银行转账。</TXT></ALTERATION_DESC></ALTERATION_DESCS><COL_DESC" +
                ">险种名称               </COL_DESC><PRODUCT_DETAILS><PRODUCT_DETAIL><PRODUCT_NAME></PRODUCT_NAME" +
                "><AMOUNT_DESC><![CDATA[\"\"]]></AMOUNT_DESC></PRODUCT_DETAIL><PRODUCT_DETAIL></PRODUCT_DETAIL" +
                "></PRODUCT_DETAILS><RETURN_PREM>0.00元</RETURN_PREM><BANK_AND_SAVING_AGENCY></BANK_AND_SAVING_AGENCY" +
                "><BUSINESS_GROUP>江苏分公司南京本部周长鹏营业区江苏分公司南京本部周长鹏营业区吴亚男营业部</BUSINESS_GROUP><MARKETING_GROUP" +
                ">江苏分公司南京本部营业部</MARKETING_GROUP><AGENT_NAME>吴冬梅</AGENT_NAME><AGENT_CODE><![CDATA[\"200008448" +
                "\"]]></AGENT_CODE><PRINT_ORGAN>天安人寿保险股份有限公司</PRINT_ORGAN><PRINT_DATE>2018年08月21日</PRINT_DATE" +
                "></LETTER_DETAIL><LETTER_DETAIL></LETTER_DETAIL></LETTER_DETAILS></ROOT>");
        jsonArray.add(json);
        paramData.put("datas",jsonArray);
//        String result = HttpUtil.post("http://10.0.21.73:8080/core/printReport", paramData.toJSONString());
//        System.out.println(result);
//        HttpDownload.download("http://10.0.21.73:8080/core/printAndViewReport",paramData ,"C:\\Users\\Administrator\\Desktop" +
//                "\\wkhtmltopdf\\1.pdf");
        HttpDownload.download("http://localhost:8080/core/printAndViewReport",paramData ,
                "C:\\Users\\Administrator\\Desktop" +
                "\\wkhtmltopdf\\2.pdf");
    }

}
